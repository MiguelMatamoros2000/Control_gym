/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package img;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;


public class configuracion_logo extends javax.swing.JPanel {
  
  String ruta = "";
  
  
  public configuracion_logo(int x, int y, String ruta){
    
    this.setSize(x,y);
    this.ruta = ruta;
  }
  
  
  public void paint( Graphics g ){
    
    Dimension height = getSize();
    ImageIcon img = new ImageIcon(getClass().getResource(ruta));
    
    g.drawImage(img.getImage(), 0, 0, height.width, height.height , null);
    
    setOpaque(false);
    super.paintComponent(g);
    
  }
  
  
  
}
