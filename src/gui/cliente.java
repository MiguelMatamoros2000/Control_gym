 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import com.sun.glass.events.KeyEvent;
import conexionBaseDatos.consultasTablaClientes;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
<<<<<<< HEAD
=======
import javax.swing.JOptionPane;
>>>>>>> luis
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import rojerusan.RSNotifyAnimated;
import utilidades.TextPrompt;
import utilidades.notificaciones;
import utilidades.configuracionXml;

/**
 *
 * @author L
 */
public class cliente extends javax.swing.JInternalFrame {
  
  configuracionXml con = new configuracionXml();
  consultasTablaClientes conexion = new consultasTablaClientes(con.getConexion().getConexion());
  notificaciones notificaciones = new notificaciones();
  
  private Timer timer;
  private ActionListener ac;
  private int incremento = 0;
  
   private Timer timer;
  private ActionListener ac;
  private int incremento = 0;
  
  
  static String nombre_cliente = "";
  static String apellidos_cliente = "";
  static String id_cliente = "";
  int selecion = 0;
  

  
  
  //en este metodo inicializamos las mascaras de entrada que contienen cada caja de texto 
  public void mascara_entrada(){
    /*mascara de entrada de nombre */
    TextPrompt placeholder_nombre = new TextPrompt("Nombre", nombre);
      placeholder_nombre.changeAlpha(0.75f);
      placeholder_nombre.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_apellidos = new TextPrompt("Apellidos", apellidos);
      placeholder_apellidos.changeAlpha(0.75f);
      placeholder_apellidos.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_telefono = new TextPrompt("Telefono", telefono);
      placeholder_telefono.changeAlpha(0.75f);
      placeholder_telefono.changeStyle(Font.BOLD);
    
    TextPrompt placeholder_telefono_emergencia = new TextPrompt("Telefono Emergencia", telefonoEmergencia);
      placeholder_telefono_emergencia.changeAlpha(0.75f);
      placeholder_telefono_emergencia.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_correo = new TextPrompt("Correo electronico", correo);
      placeholder_correo.changeAlpha(0.75f);
      placeholder_correo.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_red = new TextPrompt("Red social", red);
      placeholder_red.changeAlpha(0.75f);
      placeholder_red.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_clave = new TextPrompt("Clave", clave);
      placeholder_clave.changeAlpha(0.75f);
      placeholder_clave.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_busqueda = new TextPrompt("Buscar", busqueda);
      placeholder_busqueda.changeAlpha(0.75f);
      placeholder_busqueda.changeStyle(Font.BOLD);
      
  }
  
  private void guadra_cliente(){
    
    String _nombre = nombre.getText();
    String _apellido = apellidos.getText();
    String _telefono = telefono.getText();
    String _telefonoEmergencia = telefonoEmergencia.getText();
    String _correo = correo.getText();
    String _redsocial = red.getText();
    String _clave = clave.getText();
    byte[] imagen = jPanelWebCam1.getBytes();
  
    try{
    
    if( !_nombre.equals("") && !_apellido.equals("") && !_telefono.equals("") && !_telefonoEmergencia.equals("") 
      && !_correo.equals("") && !_redsocial.equals("") && !_clave.equals("") ){
      
      
      Map<String,Object> datos = new HashMap<>();
      datos.put( "nombre" , _nombre );
      datos.put( "apellido" , _apellido );
      datos.put( "telefono" , _telefono );
      datos.put( "telefono_emergencia" , _telefonoEmergencia );
      datos.put( "correo" , _correo );
      datos.put( "red_social" , _redsocial );
      datos.put( "clave_llave_secundaria" , _clave );
      datos.put("imagen", imagen);
      
      
      boolean resultado = conexion.ingresar(datos);
      
      
      if(resultado){
      
        nombre_cliente = nombre.getText();
        apellidos_cliente = apellidos.getText(); 
        id_cliente = conexion.seleccionar_id(datos);
        notificaciones.mensajeCorrecto("El cliente "+ _nombre + " "+ _apellido +" se registro correctamente");
        limpiar(); 
        altaClienteMembresia _altaClienteMembresia = new altaClienteMembresia();
        _altaClienteMembresia.setVisible(true);
        _altaClienteMembresia.pack();
      
        selecionarListaClientes();
        System.out.println("funciono");
        
        
      }else{
        
        notificaciones.mensajeError("El cliente "+ _nombre + " "+ _apellido +" no se logro registrar");
        
      }
      
      
    }else{
      notificaciones.mensajeAdvertencia("Porfavor llene de forma correcta todo el formulario");
    }
    
    }
    catch (SQLException ex)
    {
      System.out.println("Erroro");
      Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
    
  }
  
  //metodo selecionar lista de clientes, donde recuperamos los datos de la consulta a la tabla
  public void selecionarListaClientes(){
    try 
    {
      DefaultTableModel modelo_tabla_cliente = new DefaultTableModel();
      modelo_tabla_cliente = conexion.selecionarListaClientes();
      tbClientes.setModel( modelo_tabla_cliente );
    } 
    catch (SQLException ex)
    {
      notificaciones.mensajeError("No se lograron recuperar los datos");
      Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public void selecionarEstatusMembresia(){
    
    try {
      DefaultTableModel modelo_tabla_membresia = new DefaultTableModel();
      modelo_tabla_membresia = conexion.selecionarLista();
      tbClientes.setModel(modelo_tabla_membresia);
    } catch (SQLException ex) {
      notificaciones.mensajeError("No se lograron recuperar los dato");
      Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
  }
  
  
  //en este metodo comparamos el texto del boton para decidir que accion realizar
  private void acciones_boton_guardar(){
    
    String nombre = btnGuardar.getText();
    
    if( nombre == "Guardar"){
     
      guadra_cliente();
      
    }
    
    else{
      
      actualizarCliente();
      
    }
  
    
  }
  
  //metodo menu editar clic derecho raton
  //camviamos la opcion del boton pasamos los datos al formulario
  public void editar_cliente(){
    btnGuardar.setText("Guardar cambios");
    panelPrincipalClientes.setSelectedIndex(0);
    seleccionarCliente();
    etiquetas_activas();
  }
  
  
  private void actualizarCliente()
  {
    
    String _nombre = nombre.getText();
    String _apellido = apellidos.getText();
    String _telefono = telefono.getText();
    String _telefonoEmergencia = telefonoEmergencia.getText();
    String _correo = correo.getText();
    String _redsocial = red.getText();
    String _clave = clave.getText();
    byte[] imagen = jPanelWebCam1.getBytes();
    
    try 
    {
      
    
    
    if( !_nombre.equals("") && !_apellido.equals("") && !_telefono.equals("") && !_telefonoEmergencia.equals("") 
      && !_correo.equals("") && !_redsocial.equals("") && !_clave.equals("") ){
      
      
      Map<String,Object> datos = new HashMap<>();
      datos.put( "nombre" , _nombre );
      datos.put( "apellido" , _apellido );
      datos.put( "telefono" , _telefono );
      datos.put( "telefono_emergencia" , _telefonoEmergencia );
      datos.put( "correo" , _correo );
      datos.put( "red_social" , _redsocial );
      datos.put( "clave_llave_secundaria" , _clave );
      datos.put("imagen", imagen);
        
        //comparamos el numero de la fila 
        int fila = tbClientes.getSelectedRow();
        if ( fila >= 0 ) 
        {
          //mostramos la tabla del resultado
          DefaultTableModel modelo_tabla_clientes = (DefaultTableModel)tbClientes.getModel();
          int id_fila = Integer.parseInt( modelo_tabla_clientes.getValueAt( fila, 0 ).toString() );
          //pasamos los datos empasulados de la variable valores a la classe accionesClientes
          boolean resultado = conexion.actualizar(id_fila, datos );
          //comparmaos que el valor no este vacio 
          if( resultado )
          {
          // recupermos nuetra nueva lista 
            modelo_tabla_clientes = conexion.selecionarLista();
            tbClientes.setModel( modelo_tabla_clientes );
         //madamos mensaje de exito
            notificaciones.mensajeCorrecto("El cliente " +_nombre +" "+  _apellido  + " fue acualizado correctamente" );
            selecionarListaClientes();
            btnGuardar.setText("Guardar");
            panelPrincipalClientes.setSelectedIndex(1);
          }
          else
          {
          notificaciones.mensajeError("El cliente " +_nombre +" "+  _apellido + " no pudo ser actualizado" );
          }
        }
      }
      else
      {
        notificaciones.mensajeAdvertencia("Llene todos los campos antes de actualizar un nuevo cliente o selecione un "
                + "cliente " );
      }
    } 
    catch (SQLException ex)
    {
    notificaciones.mensajeError( "El cliente " +_nombre +" "+  _apellido + " no pudo ser actualizado"  );
      Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  
  public void seleccionarCliente(){
     
    panelPrincipalClientes.setSelectedIndex(0);
 
    
    int fila = tbClientes.getSelectedRow();
    if ( fila >= 0 ) 
    {
      try 
      {
        DefaultTableModel modelo_tabla_clientes = (DefaultTableModel)tbClientes.getModel();
        int id_fila = Integer.parseInt( modelo_tabla_clientes.getValueAt( fila, 0 ).toString() );
        java.util.List<Object> clientes = conexion.Seleccion(id_fila );
        
        if( clientes.size() > 0 )
        {
          Map<String, Object> map = ( ( Map<String, Object> ) ( clientes.get( 0 ) ) );
          String _nombres = map.get( "nombre").toString();
          String _apellidos = map.get( "apellido").toString();
          String _correos = map.get( "correo" ).toString();
          String _telefonos = map.get( "telefono").toString();
          String _telem = map.get( "telefono_emergencia" ).toString();
          String _red = map.get( "red_social" ).toString();
          String _clave = map.get( "clave_llave_secundaria" ).toString();
          byte[] _imagen = (byte[]) map.get("foto");
                  
          nombre.setText(_nombres);
          apellidos.setText(_apellidos);
          telefono.setText(_telefonos);
          telefonoEmergencia.setText(_telem);
          correo.setText(_correos);
          red.setText(_red);
          clave.setText(_clave);
          jPanelWebCam1.setImagen(_imagen);
        }
        
        else
        {
        }
        
      } 
      catch (SQLException ex) 
      {
        notificaciones.mensajeError("No se lograron recuperar los datos");
        Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
      }
      
   }}
  
  private void asignarMembracia(){
    
    int fila = tbClientes.getSelectedRow();
    try {
      
      if(fila >= 0 ){
        
      
      DefaultTableModel modeloTablaCliente = (DefaultTableModel) tbClientes.getModel();
      int id_fila= Integer.parseInt( modeloTablaCliente.getValueAt( fila, 0 ).toString() );
      id_cliente = modeloTablaCliente.getValueAt( fila, 0 ).toString();
      java.util.List<Object> cliente = conexion.Seleccion( id_fila);
      
      if( cliente.size() > 0){
        
        Map<String, Object> map = ( ( Map<String, Object> ) ( cliente.get( 0 ) ) );
          String _nombres = map.get( "nombre").toString();
          String _apellidos = map.get( "apellido").toString();
        
        nombre_cliente = _nombres ;
        apellidos_cliente = _apellidos;
           
      }
      
      altaClienteMembresia _altaClienteMembresia = new altaClienteMembresia();
        _altaClienteMembresia.setVisible(true);
        _altaClienteMembresia.pack();
      }
    } catch (Exception e) {
      notificaciones.mensajeError("No se lograron recuperar los datos");
    }
    
    
  }
  
  private void actualizarmembresia(){
    
    int fila = tbClientes.getSelectedRow();
    try {
      
      if(fila >= 0 ){
        
      
      DefaultTableModel modeloTablaCliente = (DefaultTableModel) tbClientes.getModel();
      int id_fila= Integer.parseInt( modeloTablaCliente.getValueAt( fila, 1 ).toString() );
      id_cliente = modeloTablaCliente.getValueAt( fila, 1 ).toString();
      java.util.List<Object> cliente = conexion.Seleccion( id_fila);
      
      if( cliente.size() > 0){
        
        Map<String, Object> map = ( ( Map<String, Object> ) ( cliente.get( 0 ) ) );
          String _nombres = map.get( "nombre").toString();
          String _apellidos = map.get( "apellido").toString();
        
        nombre_cliente = _nombres ;
        apellidos_cliente = _apellidos;
        
      }
      
      altaClienteMembresia _altaClienteMembresia = new altaClienteMembresia();
        _altaClienteMembresia.setVisible(true);
        _altaClienteMembresia.pack();
      }
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se lograron recuperar los datos");
    
    }
    
  }
  
  private void eliminar_cliente(){
    
    try {
    
      int fila = tbClientes.getSelectedRow();
      if( fila >= 0 ){
        
        DefaultTableModel modeloTablaCliente = (DefaultTableModel) tbClientes.getModel();
        int id_fila = Integer.parseInt( modeloTablaCliente.getValueAt(fila, 0).toString() );
        boolean resultado = conexion.borrar(id_fila);
        
        if( resultado )
        {
          
          modeloTablaCliente = new DefaultTableModel();
          modeloTablaCliente = conexion.selecionarListaClientes();
          tbClientes.setModel(modeloTablaCliente);
          limpiar();
          notificaciones.mensajeCorrecto("El cliente fue eliminado");
          
        }else{
          
          notificaciones.mensajeError("No se logro eliminar el cliente");
          
        }
                
        
        
      }
      
      
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se lograron recuperar los datos");
      
    }
  }
  
  private void eliminar_membresia(){
    
    try {
    
      int fila = tbClientes.getSelectedRow();
      if( fila >= 0 ){
        
        DefaultTableModel modeloTablaCliente = (DefaultTableModel) tbClientes.getModel();
        int id_fila = Integer.parseInt( modeloTablaCliente.getValueAt(fila, 0).toString() );
        boolean resultado = conexion.borrar_membresia(id_fila);
        
        if( resultado )
        {
          
          modeloTablaCliente = new DefaultTableModel();
          modeloTablaCliente = conexion.selecionarListaClientes();
          tbClientes.setModel(modeloTablaCliente);
          limpiar();
          notificaciones.mensajeCorrecto("La membresia fue eliminada");
          
        }else{
          
          notificaciones.mensajeError("No se logro eliminar La membresia");
          
        }
                
        
        
      }
      
      
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se lograron recuperar los datos");
      
    }
  }
  
  private void pregunta_cofirmacion(){
    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int respuesta = JOptionPane.showConfirmDialog(null, " ¿ Estas seguro de esta accion ?", " Eliminar ", dialogo);
    
    if(respuesta == 0){
      eliminar_cliente();
    }
    
  }
  
  private void pregunta_confirmacion_eliminar_membresia(){
    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int respuesta = JOptionPane.showConfirmDialog(null, "¿ Estas seguro de esta accion ?","Borrar",dialogo);
    
    if(respuesta == 0){
      eliminar_membresia();
    }
  }
  
  
  private void limpiar(){
    
    btnGuardar.setText("Guardar");
    nombre.setText("");
    apellidos.setText("");
    telefono.setText("");
    telefonoEmergencia.setText("");
    correo.setText("");
    red.setText("");
    clave.setText("");
    
    etiquetas_desactivadas();
    
  }
  
  private void validar_numeros_telefono(java.awt.event.KeyEvent evt){
    
    
    
    char validad_numero = evt.getKeyChar();
    
    if(Character.isLetter(validad_numero)){
      getToolkit().beep();
      evt.consume();
      
    }
    
    
  }
  
  private void limitar_telefono(java.awt.event.KeyEvent evt){
    
    if (telefono.getText().length() == 15){
    evt.consume();}
    
    
  }
  
  private void limitar_telefono_emergencia(java.awt.event.KeyEvent evt){
    
    if (telefonoEmergencia.getText().length() == 15){
    evt.consume();}
    
    
  }
  
  public void mostrarEtiquetaNombre(java.awt.event.KeyEvent evt){
    
    if( evt.getKeyCode() != KeyEvent.VK_BACKSPACE ){
      
      etiquetaNombre.setVisible(true);
      
    }else{
      
      etiquetaNombre.setVisible(false);
    }
    
  }
  
  
  public void mostraEtiquetaApellidos(java.awt.event.KeyEvent evt){
    
    if( evt.getKeyCode() != KeyEvent.VK_BACKSPACE ){
      
      etiquetaApellidos.setVisible(true);
      
    }else{
      
      etiquetaApellidos.setVisible(false);
    }
    
  }
  
  public void mostrarEtiquetaTelefono(java.awt.event.KeyEvent evt){
    
    if( evt.getKeyCode() != KeyEvent.VK_BACKSPACE ){
      etiquetaTelefono.setVisible(true);
    }else{
      etiquetaTelefono.setVisible(false);
    }
    
  }
  
  public void mostrarEtiquetaTelefonoEmergencia(java.awt.event.KeyEvent evt){
    
    if( evt.getKeyCode() != KeyEvent.VK_BACKSPACE ){
      etiquetaTelefonoEmergencia.setVisible(true);
    }else{
      etiquetaTelefonoEmergencia.setVisible(false);
    }
    
  }
  
  public void mostrarEtiquetaCorreo(java.awt.event.KeyEvent evt){
    
    if(evt.getKeyCode() != KeyEvent.VK_BACKSPACE){
      etiquetaCorreo.setVisible(true);
    }else{
      etiquetaCorreo.setVisible(false);
    }
    
  }
  
  public void mostrarEtiquetaRed(java.awt.event.KeyEvent evt){
    
    if(evt.getKeyCode() != KeyEvent.VK_BACKSPACE){
      etiquetaRed.setVisible(true);
    }else{
      etiquetaRed.setVisible(false);
    }
    
  }
  
  public void mostrarEtiquetaClave(java.awt.event.KeyEvent evt){
    
    if( evt.getKeyCode() != KeyEvent.VK_BACKSPACE ){
      etiquetaClave.setVisible(true);
    }else{
      etiquetaClave.setVisible(false);
    }
    
  }
  
  
  
  //este metodo ocultara todas las eitquetas que hacen referencia a las cajas de texto
  private void etiquetas_desactivadas(){
    
    etiquetaNombre.setVisible(false);
    etiquetaApellidos.setVisible(false);
    etiquetaTelefono.setVisible(false);
    etiquetaTelefonoEmergencia.setVisible(false);
    etiquetaCorreo.setVisible(false);
    etiquetaRed.setVisible(false);
    etiquetaClave.setVisible(false);
    
  }
  
  private void etiquetas_activas(){
    
    etiquetaNombre.setVisible(true);
    etiquetaApellidos.setVisible(true);
    etiquetaTelefono.setVisible(true);
    etiquetaTelefonoEmergencia.setVisible(true);
    etiquetaCorreo.setVisible(true);
    etiquetaRed.setVisible(true);
    etiquetaClave.setVisible(true);
    
  }
  
<<<<<<< HEAD
//  public void actualizartabla(){
//    
//    ac = new ActionListener() {
//      
//
//      @Override
//      public void actionPerformed(ActionEvent e) {
//        
//        
//      
//          selecionarEstatusMembresia();
//        
//      
//      }
//      
//    };
//    
//    timer = new Timer(50, ac);
//    timer.start();
//    
//    
//  }
  
=======
>>>>>>> luis
  public void boton_muestra_memnbresia_Cliente(){
    
    
    if(btnMostrarMembresia.getText().equals("Lista de membresias")){
      
      btnMostrarMembresia.setText("Lista de clientes");
<<<<<<< HEAD
       selecionarEstatusMembresia();
       
        jMenuItemEditar.setEnabled(false);
        jMenuItemEditar.setVisible(false);
        jMenuItemEliminar.setVisible(false);
      
    } else if(btnMostrarMembresia.getText().equals("Lista de clientes")){
=======
        selecionarEstatusMembresia();
        comboBusqueda.removeAllItems();
        item_busqueda_membresia();
        selecion = 1;
        jMenuItemEditar.setEnabled(false);
        jMenuItemEditar.setVisible(false);
        jMenuItemEliminar.setVisible(false);
        jMenuItemMembresia.setVisible(true);
        jMenuItemEliminarMembresia.setVisible(true);
        jMenuItemEditarMembresia.setVisible(true);
        jMenuItemMembresiaAsignar.setVisible(false);
      
    } else if(btnMostrarMembresia.getText().equals("Lista de clientes")){
      comboBusqueda.removeAllItems();
      item_cliente_busqueda();
      selecion = 0;
>>>>>>> luis
      
      btnMostrarMembresia.setText("Lista de membresias");
      selecionarListaClientes();
      jMenuItemEditar.setEnabled(true);
      jMenuItemEditar.setVisible(true);
      jMenuItemEliminar.setVisible(true);
<<<<<<< HEAD
      jMenuItemMembresia.setVisible(true);
=======
      jMenuItemMembresia.setVisible(false);
      jMenuItemEliminarMembresia.setVisible(false);
      jMenuItemEditarMembresia.setVisible(false);
      jMenuItemMembresiaAsignar.setVisible(true);
>>>>>>> luis
      
    }
    
  }
  
<<<<<<< HEAD
=======
  private void item_cliente_busqueda(){
    
    comboBusqueda.addItem("Seleciona un campo");
    comboBusqueda.addItem("id_cliente");
    comboBusqueda.addItem("nombre");
    comboBusqueda.addItem("apellido");
    comboBusqueda.addItem("telefono");
    comboBusqueda.addItem("telefono_emergencia");
    comboBusqueda.addItem("clave_llave_secundaria");
    comboBusqueda.addItem("red_social");
    comboBusqueda.addItem("correo");
 
    
  }
  
  private void item_busqueda_membresia(){
    
    comboBusqueda.addItem("Seleciona un campo");
    comboBusqueda.addItem("membresia.id_membresia");
    comboBusqueda.addItem("cliente.id_cliente");
    comboBusqueda.addItem("cliente.nombre");
    comboBusqueda.addItem("cliente.apellido");
    comboBusqueda.addItem("cliente.clave_llave_secundaria");
    comboBusqueda.addItem("membresia.fecha");
    comboBusqueda.addItem("membresia.fecha_fin");
    comboBusqueda.addItem("membresia.estatus");
    
  }
  
  private void metodo_busqueda(){
    
    String campo = comboBusqueda.getSelectedItem().toString();
    String _busqueda = busqueda.getText();
    
    try {
    
    if(!campo.equals("Seleciona un campo") ){
    
      DefaultTableModel modelo_tabla_cliente = new DefaultTableModel();
      modelo_tabla_cliente = conexion.busqueda(selecion, campo, _busqueda);
      tbClientes.setModel( modelo_tabla_cliente );
    
    }else{
     notificaciones.mensjeInformacion("Selecione un campo o coloque su buesqueda");
    }
    
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se encontraron datos ");
    }
  }
  
>>>>>>> luis
  
  public cliente() {
    initComponents();
    mascara_entrada();
    selecionarListaClientes();
    etiquetas_desactivadas();
<<<<<<< HEAD
    //actualizartabla();
=======
    boton_muestra_memnbresia_Cliente();
>>>>>>> luis
    conexion.actualizar_estatus();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
   * content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    clic_mouse = new javax.swing.JPopupMenu();
    jMenuItemEditar = new javax.swing.JMenuItem();
    jMenuItemEliminar = new javax.swing.JMenuItem();
    jMenuItemMembresia = new javax.swing.JMenuItem();
<<<<<<< HEAD
=======
    jMenuItemMembresiaAsignar = new javax.swing.JMenuItem();
    jMenuItemEliminarMembresia = new javax.swing.JMenuItem();
    jMenuItemEditarMembresia = new javax.swing.JMenuItem();
    jPanelWebCambBeanInfo1 = new JPanelWebCam.JPanelWebCambBeanInfo();
>>>>>>> luis
    jPanel1 = new javax.swing.JPanel();
    panelPrincipalClientes = new javax.swing.JTabbedPane();
    jPanel2 = new javax.swing.JPanel();
    nombre = new javax.swing.JTextField();
    apellidos = new javax.swing.JTextField();
    jPanel5 = new javax.swing.JPanel();
    telefono = new javax.swing.JTextField();
    telefonoEmergencia = new javax.swing.JTextField();
    jPanel7 = new javax.swing.JPanel();
    correo = new javax.swing.JTextField();
    red = new javax.swing.JTextField();
    jPanel11 = new javax.swing.JPanel();
    clave = new javax.swing.JTextField();
    jPanel12 = new javax.swing.JPanel();
    btnGuardar = new javax.swing.JButton();
    btnLimpiar = new javax.swing.JButton();
    etiquetaNombre = new javax.swing.JLabel();
    jPanel13 = new javax.swing.JPanel();
    jPanel14 = new javax.swing.JPanel();
    jPanel15 = new javax.swing.JPanel();
    etiquetaApellidos = new javax.swing.JLabel();
    etiquetaTelefono = new javax.swing.JLabel();
    etiquetaTelefonoEmergencia = new javax.swing.JLabel();
    etiquetaCorreo = new javax.swing.JLabel();
    etiquetaRed = new javax.swing.JLabel();
    etiquetaClave = new javax.swing.JLabel();
    jPanelWebCam1 = new JPanelWebCam.JPanelWebCam();
    jLabel1 = new javax.swing.JLabel();
    jButton1 = new javax.swing.JButton();
    jPanel3 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tbClientes = new javax.swing.JTable();
    jButton5 = new javax.swing.JButton();
<<<<<<< HEAD
    jTextField8 = new javax.swing.JTextField();
    jComboBox2 = new javax.swing.JComboBox();
    btnMostrarMembresia = new javax.swing.JButton();
=======
    busqueda = new javax.swing.JTextField();
    comboBusqueda = new javax.swing.JComboBox();
    btnMostrarMembresia = new javax.swing.JButton();
    jPanel4 = new javax.swing.JPanel();
>>>>>>> luis

    jMenuItemEditar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEditar.setText("Editar");
    jMenuItemEditar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEditarActionPerformed(evt);
      }
    });
    clic_mouse.add(jMenuItemEditar);

    jMenuItemEliminar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEliminar.setText("Eliminar");
    jMenuItemEliminar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEliminarActionPerformed(evt);
      }
    });
    clic_mouse.add(jMenuItemEliminar);

    jMenuItemMembresia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
<<<<<<< HEAD
    jMenuItemMembresia.setText("Membresia");
=======
    jMenuItemMembresia.setText("Renovar membresia");
>>>>>>> luis
    jMenuItemMembresia.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemMembresiaActionPerformed(evt);
      }
    });
    clic_mouse.add(jMenuItemMembresia);

<<<<<<< HEAD
=======
    jMenuItemMembresiaAsignar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemMembresiaAsignar.setText("Asignar membresia");
    jMenuItemMembresiaAsignar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemMembresiaAsignarActionPerformed(evt);
      }
    });
    clic_mouse.add(jMenuItemMembresiaAsignar);

    jMenuItemEliminarMembresia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEliminarMembresia.setText("Eliminar membresia");
    jMenuItemEliminarMembresia.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEliminarMembresiaActionPerformed(evt);
      }
    });
    clic_mouse.add(jMenuItemEliminarMembresia);

    jMenuItemEditarMembresia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEditarMembresia.setText("Editar membresia");
    clic_mouse.add(jMenuItemEditarMembresia);

>>>>>>> luis
    setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
    setResizable(true);

    jPanel1.setBackground(new java.awt.Color(30, 30, 30));

    panelPrincipalClientes.setBackground(new java.awt.Color(249, 133, 42));
    panelPrincipalClientes.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N

    jPanel2.setBackground(new java.awt.Color(30, 30, 30));

    nombre.setBackground(new java.awt.Color(30, 30, 30));
    nombre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    nombre.setForeground(new java.awt.Color(255, 255, 255));
    nombre.setBorder(null);
    nombre.setCaretColor(new java.awt.Color(255, 255, 255));
    nombre.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyTyped(java.awt.event.KeyEvent evt) {
        nombreKeyTyped(evt);
      }
      public void keyReleased(java.awt.event.KeyEvent evt) {
        nombreKeyReleased(evt);
      }
    });

    apellidos.setBackground(new java.awt.Color(30, 30, 30));
    apellidos.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    apellidos.setForeground(new java.awt.Color(255, 255, 255));
    apellidos.setBorder(null);
    apellidos.setCaretColor(new java.awt.Color(255, 255, 255));
    apellidos.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        apellidosKeyReleased(evt);
      }
    });

    jPanel5.setBackground(new java.awt.Color(249, 133, 42));
    jPanel5.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    telefono.setBackground(new java.awt.Color(30, 30, 30));
    telefono.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    telefono.setForeground(new java.awt.Color(255, 255, 255));
    telefono.setBorder(null);
    telefono.setCaretColor(new java.awt.Color(255, 255, 255));
    telefono.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyTyped(java.awt.event.KeyEvent evt) {
        telefonoKeyTyped(evt);
      }
      public void keyReleased(java.awt.event.KeyEvent evt) {
        telefonoKeyReleased(evt);
      }
    });

    telefonoEmergencia.setBackground(new java.awt.Color(30, 30, 30));
    telefonoEmergencia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    telefonoEmergencia.setForeground(new java.awt.Color(255, 255, 255));
    telefonoEmergencia.setBorder(null);
    telefonoEmergencia.setCaretColor(new java.awt.Color(255, 255, 255));
    telefonoEmergencia.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyTyped(java.awt.event.KeyEvent evt) {
        telefonoEmergenciaKeyTyped(evt);
      }
      public void keyReleased(java.awt.event.KeyEvent evt) {
        telefonoEmergenciaKeyReleased(evt);
      }
    });

    jPanel7.setBackground(new java.awt.Color(249, 133, 42));
    jPanel7.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
    jPanel7.setLayout(jPanel7Layout);
    jPanel7Layout.setHorizontalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel7Layout.setVerticalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    correo.setBackground(new java.awt.Color(30, 30, 30));
    correo.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    correo.setForeground(new java.awt.Color(255, 255, 255));
    correo.setBorder(null);
    correo.setCaretColor(new java.awt.Color(255, 255, 255));
    correo.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        correoKeyReleased(evt);
      }
    });

    red.setBackground(new java.awt.Color(30, 30, 30));
    red.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    red.setForeground(new java.awt.Color(255, 255, 255));
    red.setBorder(null);
    red.setCaretColor(new java.awt.Color(255, 255, 255));
    red.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        redKeyReleased(evt);
      }
    });

    jPanel11.setBackground(new java.awt.Color(249, 133, 42));
    jPanel11.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
    jPanel11.setLayout(jPanel11Layout);
    jPanel11Layout.setHorizontalGroup(
      jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel11Layout.setVerticalGroup(
      jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    clave.setBackground(new java.awt.Color(30, 30, 30));
    clave.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    clave.setForeground(new java.awt.Color(255, 255, 255));
    clave.setBorder(null);
    clave.setCaretColor(new java.awt.Color(255, 255, 255));
    clave.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        claveKeyReleased(evt);
      }
    });

    jPanel12.setBackground(new java.awt.Color(249, 133, 42));
    jPanel12.setPreferredSize(new java.awt.Dimension(340, 2));

    javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
    jPanel12.setLayout(jPanel12Layout);
    jPanel12Layout.setHorizontalGroup(
      jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 340, Short.MAX_VALUE)
    );
    jPanel12Layout.setVerticalGroup(
      jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    btnGuardar.setBackground(new java.awt.Color(249, 133, 42));
    btnGuardar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
    btnGuardar.setText("Guardar");
    btnGuardar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGuardarActionPerformed(evt);
      }
    });

    btnLimpiar.setBackground(new java.awt.Color(249, 133, 42));
    btnLimpiar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
    btnLimpiar.setText("Limpiar");
    btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnLimpiarActionPerformed(evt);
      }
    });

    etiquetaNombre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaNombre.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaNombre.setText("Nombre");

    jPanel13.setBackground(new java.awt.Color(249, 133, 42));
    jPanel13.setPreferredSize(new java.awt.Dimension(340, 2));

    javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
    jPanel13.setLayout(jPanel13Layout);
    jPanel13Layout.setHorizontalGroup(
      jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel13Layout.setVerticalGroup(
      jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );

    jPanel14.setBackground(new java.awt.Color(249, 133, 42));
    jPanel14.setPreferredSize(new java.awt.Dimension(340, 2));

    javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
    jPanel14.setLayout(jPanel14Layout);
    jPanel14Layout.setHorizontalGroup(
      jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel14Layout.setVerticalGroup(
      jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );

    jPanel15.setBackground(new java.awt.Color(249, 133, 42));
    jPanel15.setPreferredSize(new java.awt.Dimension(340, 2));

    javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
    jPanel15.setLayout(jPanel15Layout);
    jPanel15Layout.setHorizontalGroup(
      jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel15Layout.setVerticalGroup(
      jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );

    etiquetaApellidos.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaApellidos.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaApellidos.setText("Apellidos");

    etiquetaTelefono.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaTelefono.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaTelefono.setText("Telefono");

    etiquetaTelefonoEmergencia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaTelefonoEmergencia.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaTelefonoEmergencia.setText("Telefono de emergencia ");

    etiquetaCorreo.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaCorreo.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaCorreo.setText("Correo");

    etiquetaRed.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaRed.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaRed.setText("Red social");

    etiquetaClave.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 12)); // NOI18N
    etiquetaClave.setForeground(new java.awt.Color(255, 255, 255));
    etiquetaClave.setText("Clave");

    jPanelWebCam1.setBackground(new java.awt.Color(30, 30, 30));
    jPanelWebCam1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    jPanelWebCam1.setFONDO(false);

    jLabel1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(153, 153, 153));
    jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel1.setText("Click para tomar fotografia");

    javax.swing.GroupLayout jPanelWebCam1Layout = new javax.swing.GroupLayout(jPanelWebCam1);
    jPanelWebCam1.setLayout(jPanelWebCam1Layout);
    jPanelWebCam1Layout.setHorizontalGroup(
      jPanelWebCam1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanelWebCam1Layout.createSequentialGroup()
        .addGap(39, 39, 39)
        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(45, Short.MAX_VALUE))
    );
    jPanelWebCam1Layout.setVerticalGroup(
      jPanelWebCam1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanelWebCam1Layout.createSequentialGroup()
        .addGap(175, 175, 175)
        .addComponent(jLabel1)
        .addContainerGap(179, Short.MAX_VALUE))
    );

    jButton1.setBackground(new java.awt.Color(249, 133, 42));
    jButton1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton1.setForeground(new java.awt.Color(255, 255, 255));
    jButton1.setText("Eliminar foto");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap(297, Short.MAX_VALUE)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(clave, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(etiquetaClave))
        .addGap(754, 754, 754))
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
        .addGap(65, 65, 65)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(correo, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addComponent(telefono)
                .addComponent(nombre)
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE))
              .addComponent(etiquetaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(etiquetaTelefono)
              .addComponent(etiquetaCorreo))
            .addGap(69, 69, 69)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(etiquetaApellidos)
              .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(etiquetaTelefonoEmergencia)
                  .addComponent(etiquetaRed)
                  .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                      .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                      .addComponent(red, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                      .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                      .addComponent(apellidos)
                      .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                      .addComponent(telefonoEmergencia))))
                .addGap(57, 57, 57)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(jPanelWebCam1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(33, 33, 33))))))
          .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(55, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addGap(18, 18, 18)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(etiquetaNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(etiquetaApellidos))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(apellidos, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
              .addComponent(nombre, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(18, 18, 18)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(etiquetaTelefono)
              .addComponent(etiquetaTelefonoEmergencia))
            .addGap(3, 3, 3)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(telefonoEmergencia, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
              .addComponent(telefono, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(25, 25, 25)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(etiquetaCorreo)
              .addComponent(etiquetaRed))
            .addGap(7, 7, 7)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(correo, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
              .addComponent(red))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(18, 18, 18)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(etiquetaClave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clave, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(191, 191, 191)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                  .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(jPanelWebCam1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButton1)))
        .addContainerGap(113, Short.MAX_VALUE))
    );

    jPanelWebCam1.getAccessibleContext().setAccessibleName("");
    jPanelWebCam1.getAccessibleContext().setAccessibleDescription("");

    panelPrincipalClientes.addTab("Formulario de registro", jPanel2);

    jPanel3.setBackground(new java.awt.Color(30, 30, 30));

    tbClientes.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    tbClientes.setComponentPopupMenu(clic_mouse);
    jScrollPane1.setViewportView(tbClientes);

    jButton5.setBackground(new java.awt.Color(249, 133, 42));
    jButton5.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton5.setForeground(new java.awt.Color(255, 255, 255));
    jButton5.setText("Buscar");
    jButton5.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton5ActionPerformed(evt);
      }
    });

    busqueda.setBackground(new java.awt.Color(30, 30, 30));
    busqueda.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    busqueda.setForeground(new java.awt.Color(255, 255, 255));
    busqueda.setBorder(null);
    busqueda.setCaretColor(new java.awt.Color(255, 255, 255));
    busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        busquedaKeyReleased(evt);
      }
    });

    comboBusqueda.setBackground(new java.awt.Color(249, 133, 42));
    comboBusqueda.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    comboBusqueda.setForeground(new java.awt.Color(255, 255, 255));
    comboBusqueda.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

    btnMostrarMembresia.setBackground(new java.awt.Color(249, 133, 42));
    btnMostrarMembresia.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnMostrarMembresia.setForeground(new java.awt.Color(255, 255, 255));
    btnMostrarMembresia.setText("Lista de membresias");
    btnMostrarMembresia.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnMostrarMembresiaActionPerformed(evt);
      }
    });

    jPanel4.setBackground(new java.awt.Color(249, 133, 42));

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 3, Short.MAX_VALUE)
    );

    btnMostrarMembresia.setText("Lista de membresias");
    btnMostrarMembresia.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnMostrarMembresiaActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1391, Short.MAX_VALUE)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(btnMostrarMembresia, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
<<<<<<< HEAD
        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
=======
        .addComponent(comboBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
>>>>>>> luis
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(busqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 633, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
        .addContainerGap()
<<<<<<< HEAD
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButton5)
          .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(btnMostrarMembresia))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE))
=======
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(comboBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addComponent(btnMostrarMembresia)))
          .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE))
>>>>>>> luis
    );

    panelPrincipalClientes.addTab("Lista de clientes", jPanel3);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(panelPrincipalClientes)
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(panelPrincipalClientes))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
    // TODO add your handling code here:
    acciones_boton_guardar();
  }//GEN-LAST:event_btnGuardarActionPerformed

  private void jMenuItemEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEditarActionPerformed
    // TODO add your handling code here:
    editar_cliente();
  }//GEN-LAST:event_jMenuItemEditarActionPerformed

  private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    // TODO add your handling code here:
    limpiar();
  }//GEN-LAST:event_btnLimpiarActionPerformed

  private void jMenuItemEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarActionPerformed
    // TODO add your handling code here:
    pregunta_cofirmacion();
  }//GEN-LAST:event_jMenuItemEliminarActionPerformed

  private void telefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telefonoKeyTyped
    // TODO add your handling code here:
    
    validar_numeros_telefono(evt);
    limitar_telefono(evt);
    
  }//GEN-LAST:event_telefonoKeyTyped

  private void telefonoEmergenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telefonoEmergenciaKeyTyped
    // TODO add your handling code here:
    
    validar_numeros_telefono(evt);
    limitar_telefono_emergencia(evt);
  }//GEN-LAST:event_telefonoEmergenciaKeyTyped

  private void nombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreKeyTyped
    // TODO add your handling code here:
    
    mostrarEtiquetaNombre(evt);
    
  }//GEN-LAST:event_nombreKeyTyped

  private void nombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombreKeyReleased
    // TODO add your handling code here:
    mostrarEtiquetaNombre(evt);
  }//GEN-LAST:event_nombreKeyReleased

  private void telefonoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telefonoKeyReleased
    // TODO add your handling code here:
    mostrarEtiquetaTelefono(evt);
  }//GEN-LAST:event_telefonoKeyReleased

  private void correoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_correoKeyReleased
    // TODO add your handling code here:
    
    mostrarEtiquetaCorreo(evt);
    
  }//GEN-LAST:event_correoKeyReleased

  private void telefonoEmergenciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telefonoEmergenciaKeyReleased
    // TODO add your handling code here:
    mostrarEtiquetaTelefonoEmergencia(evt);
  }//GEN-LAST:event_telefonoEmergenciaKeyReleased

  private void redKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_redKeyReleased
    // TODO add your handling code here:
    mostrarEtiquetaRed(evt);
  }//GEN-LAST:event_redKeyReleased

  private void claveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_claveKeyReleased
    // TODO add your handling code here:
    mostrarEtiquetaClave(evt);
  }//GEN-LAST:event_claveKeyReleased

  private void apellidosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_apellidosKeyReleased
    // TODO add your handling code here:
    mostraEtiquetaApellidos(evt);
  }//GEN-LAST:event_apellidosKeyReleased

  private void btnMostrarMembresiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarMembresiaActionPerformed
    // TODO add your handling code here:
   boton_muestra_memnbresia_Cliente();
  }//GEN-LAST:event_btnMostrarMembresiaActionPerformed

  private void jMenuItemMembresiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMembresiaActionPerformed
    // TODO add your handling code here:
<<<<<<< HEAD
  }//GEN-LAST:event_jMenuItemMembresiaActionPerformed

=======
     actualizarmembresia();
  }//GEN-LAST:event_jMenuItemMembresiaActionPerformed

  private void jMenuItemMembresiaAsignarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMembresiaAsignarActionPerformed
    // TODO add your handling code here:
    asignarMembracia();
  }//GEN-LAST:event_jMenuItemMembresiaAsignarActionPerformed

  private void jMenuItemEliminarMembresiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarMembresiaActionPerformed
    // TODO add your handling code here:
    pregunta_confirmacion_eliminar_membresia();
  }//GEN-LAST:event_jMenuItemEliminarMembresiaActionPerformed

  private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
    // TODO add your handling code here:
    metodo_busqueda();
  }//GEN-LAST:event_jButton5ActionPerformed

  private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
    // TODO add your handling code here:
    metodo_busqueda();
  }//GEN-LAST:event_busquedaKeyReleased

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    // TODO add your handling code here:
    this.jPanelWebCam1.setImagenNull();
    
  }//GEN-LAST:event_jButton1ActionPerformed

>>>>>>> luis

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JTextField apellidos;
  private javax.swing.JButton btnGuardar;
  private javax.swing.JButton btnLimpiar;
  private javax.swing.JButton btnMostrarMembresia;
<<<<<<< HEAD
=======
  private javax.swing.JTextField busqueda;
>>>>>>> luis
  private javax.swing.JTextField clave;
  private javax.swing.JPopupMenu clic_mouse;
  private javax.swing.JComboBox comboBusqueda;
  private javax.swing.JTextField correo;
  private javax.swing.JLabel etiquetaApellidos;
  private javax.swing.JLabel etiquetaClave;
  private javax.swing.JLabel etiquetaCorreo;
  private javax.swing.JLabel etiquetaNombre;
  private javax.swing.JLabel etiquetaRed;
  private javax.swing.JLabel etiquetaTelefono;
  private javax.swing.JLabel etiquetaTelefonoEmergencia;
  private javax.swing.JButton jButton1;
  private javax.swing.JButton jButton5;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JMenuItem jMenuItemEditar;
  private javax.swing.JMenuItem jMenuItemEditarMembresia;
  private javax.swing.JMenuItem jMenuItemEliminar;
<<<<<<< HEAD
  private javax.swing.JMenuItem jMenuItemMembresia;
=======
  private javax.swing.JMenuItem jMenuItemEliminarMembresia;
  private javax.swing.JMenuItem jMenuItemMembresia;
  private javax.swing.JMenuItem jMenuItemMembresiaAsignar;
>>>>>>> luis
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel11;
  private javax.swing.JPanel jPanel12;
  private javax.swing.JPanel jPanel13;
  private javax.swing.JPanel jPanel14;
  private javax.swing.JPanel jPanel15;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JPanel jPanel7;
  private JPanelWebCam.JPanelWebCam jPanelWebCam1;
  private JPanelWebCam.JPanelWebCambBeanInfo jPanelWebCambBeanInfo1;
  private javax.swing.JScrollPane jScrollPane1;
<<<<<<< HEAD
  private javax.swing.JTextField jTextField8;
=======
>>>>>>> luis
  private javax.swing.JTextField nombre;
  private javax.swing.JTabbedPane panelPrincipalClientes;
  private javax.swing.JTextField red;
  private javax.swing.JTable tbClientes;
  private javax.swing.JTextField telefono;
  private javax.swing.JTextField telefonoEmergencia;
  // End of variables declaration//GEN-END:variables
}
