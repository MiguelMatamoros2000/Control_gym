/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import conexionBaseDatos.consultaGuardarMembresia;
import static gui.cliente.id_cliente;
import static gui.cliente.nombre_cliente;
import java.awt.event.ItemEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import modelo.modelo_membresias;
import utilidades.configuracionXml;
import utilidades.notificaciones;

/**
 *
 * @author L
 */
public class altaClienteMembresia extends javax.swing.JFrame {

  /**
   * Creates new form altaClienteMembresia
   */
  
  configuracionXml con = new configuracionXml();
  consultaGuardarMembresia conexion = new consultaGuardarMembresia(con.getConexion().getConexion());
  notificaciones notificaciones = new notificaciones();
  
  protected String fecha = "";
  protected String fecha_sql = "",fecha_sql_fin = "", id_cliente = "" ;
  protected int cantidad = 0, dia = 0, id_membresia = 0;
  protected double precio = 0;
  private final String ruta = "/img/";
  
  SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
  
  private void configuracionFrame(){
    
    try {
      cliente _cliente = new cliente();
      jLabelNombre.setText(_cliente.nombre_cliente);
      jLabelApellidos.setText(_cliente.apellidos_cliente);
      id_cliente = _cliente.id_cliente;
      
      setLocationRelativeTo(null);
      
      ImageIcon icono_wrodpad = new ImageIcon(this.getClass().getResource(ruta + "logo_inicial.jpeg"));
      setIconImage(icono_wrodpad.getImage());
      
      Calendar _fecha_inicio = Calendar.getInstance();
      _fecha_inicio.add(Calendar.DATE, dia);
      jLabel4.setText(String.format("%1$tY/%1$tm/%1$td", _fecha_inicio.getTime()));
      
      
      String fecha_ini = "";
      fecha_ini = jLabel4.getText();
      Date fecha_date_inico = formato.parse(fecha_ini);
      
      System.out.println(fecha_ini);
      Date recupera_fecha_fin = _fecha_inicio.getTime();
      
      long fecha_recupera_inicio = recupera_fecha_fin.getTime();
      java.sql.Date fecha_fin = new java.sql.Date(fecha_recupera_inicio);
      fecha_sql_fin = fecha_fin.toString();
      
      
    } catch (ParseException ex) {
      Logger.getLogger(altaClienteMembresia.class.getName()).log(Level.SEVERE, null, ex);
    }
    
  }
  
  
  private void fecha(){

    Date fechaDate, fecha_date_inico;
   
    
     Calendar _fecha = Calendar.getInstance();
      _fecha.add(Calendar.DATE, dia);
      
     
      
    
    
    try {
      cantidad_membresia.setValue(1);
      
     
      
      jLabelFecha.setText(String.format("%1$tY/%1$tm/%1$td", _fecha.getTime()));
      jLabelPrecio.setText(String.valueOf(precio));
      
      
      fecha = jLabelFecha.getText();
      
      
      
      fechaDate = formato.parse(fecha);
      
      
      System.out.println(fecha);
      
      
      Date recupera_fecha_inicio = _fecha.getTime();
      
      
      long recupera_fecha = recupera_fecha_inicio.getTime();
      java.sql.Date fechas_inicio = new java.sql.Date(recupera_fecha);
      fecha_sql = fechas_inicio.toString();
      
      
   
      
      System.out.println(fecha_sql + " " + fecha_sql_fin);
  
    } catch (ParseException ex) {
      Logger.getLogger(altaClienteMembresia.class.getName()).log(Level.SEVERE, null, ex);
    }

  }
  
  private void cantidades_precio(){
    
    Date fechaDate;
    
    int nuevo_dia = 0;
    
    int valor_cantidad = (int) cantidad_membresia.getValue();
    
    if( dia >= 1){
      
      nuevo_dia = dia * valor_cantidad;
      System.out.println(nuevo_dia);
      double gg = precio * valor_cantidad ; 
      System.out.println( gg );
      jLabelPrecio.setText(String.valueOf(gg));
    }else{
    
      nuevo_dia = dia + valor_cantidad;
    
    }
    try {
      
      Calendar _fecha = Calendar.getInstance();
      _fecha.add(Calendar.DATE, nuevo_dia);
      
      jLabelFecha.setText(String.format("%1$tY/%1$tm/%1$td", _fecha.getTime()));
      fecha = jLabelFecha.getText();
      
      fechaDate = formato.parse(fecha);
      
      Date recupera_fecha_inicio = _fecha.getTime();
      
      long recupera_fecha = recupera_fecha_inicio.getTime();
      java.sql.Date fechas_inicio = new java.sql.Date(recupera_fecha);
      fecha_sql = fechas_inicio.toString();
      
      
    } catch (Exception e) {
      
    }
    
  }
  
  
  
  private void guadar_alta(){
    
    try {
      
      if( !fecha_sql.equals("") && !fecha_sql_fin.equals("") && !id_cliente.equals("")  ){
        
        Map<String, Object> datos = new HashMap<>();
        datos.put( "fecha_inicio", fecha_sql_fin );
        datos.put( "fecha_fin", fecha_sql);
        datos.put("id_membresia_datos", id_membresia );
        datos.put( "id_cliente" , id_cliente );
        datos.put("no_membrasia", cantidad_membresia.getValue() );
        
        
        
        boolean resultado = conexion.ingresar(datos);
        
        if ( resultado ){
          notificaciones.mensajeCorrecto("La membresia a sido adquirida");
          this.dispose();
          
        }else{
          
          notificaciones.mensajeError("No se logo otorgar membresía");
          
        }
        
        
      }else{
        
        notificaciones.mensajeAdvertencia("Faltan datos por llenar");
        
      }
      
      
    } catch (Exception e) {
      
      notificaciones.mensajeError("ocurrio un error en el momento de guardar los datos");
    
    }
   
  }
  
  
  
  private void llenar_combo_membresias(){
    
    modelo_membresias contenido = new modelo_membresias( con.getConexion().getConexion() );
    DefaultComboBoxModel modeloMembresia = new DefaultComboBoxModel(contenido.mostrarmodelo());
    jbMembresia.setModel(modeloMembresia);
    
  }
  
  private void id_membresia(java.awt.event.ItemEvent evt){
    
    if( evt.getStateChange() == ItemEvent.SELECTED ){
      
      modelo_membresias _membresia = (modelo_membresias) jbMembresia.getSelectedItem();
      id_membresia = _membresia.getId();
      
      dia = conexion.selecionar_dias(id_membresia);
      precio = conexion.precio(id_membresia);
      fecha();
      System.out.println( precio );
    }
    
  }
  
  private void cerrar(){
    
    dispose();
  }
  
  private void confirmar(){
    

    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int resultado = JOptionPane.showConfirmDialog(null, " Confime para continuar ", "Salir" , dialogo);
    
    
    if(resultado == 0){
      
      guadar_alta();
      
    }
    
  }
  
  
  public altaClienteMembresia() {
    initComponents();
    configuracionFrame();
    llenar_combo_membresias();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
   * content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new javax.swing.JScrollPane();
    jTable1 = new javax.swing.JTable();
    jPanel2 = new javax.swing.JPanel();
    jTabbedPane1 = new javax.swing.JTabbedPane();
    jPanel1 = new javax.swing.JPanel();
    rSLabelHora1 = new rojeru_san.componentes.RSLabelHora();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jbMembresia = new javax.swing.JComboBox();
    jButton1 = new javax.swing.JButton();
    jButton2 = new javax.swing.JButton();
    jLabelNombre = new javax.swing.JLabel();
    jLabelApellidos = new javax.swing.JLabel();
    jPanel5 = new javax.swing.JPanel();
    jPanel6 = new javax.swing.JPanel();
    jPanel7 = new javax.swing.JPanel();
    jLabel3 = new javax.swing.JLabel();
    jLabelFecha = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabelPrecio = new javax.swing.JLabel();
    cantidad_membresia = new javax.swing.JSpinner();
    jLabel7 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();

    jTable1.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    jScrollPane1.setViewportView(jTable1);

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setUndecorated(true);
    setResizable(false);

    jPanel2.setBackground(new java.awt.Color(34, 34, 34));

    jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 3));

    jPanel1.setBackground(new java.awt.Color(49, 49, 49));

    rSLabelHora1.setForeground(new java.awt.Color(255, 255, 255));

    jLabel1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(255, 255, 255));
    jLabel1.setText("Nombre :");

    jLabel2.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel2.setForeground(new java.awt.Color(255, 255, 255));
    jLabel2.setText("Apellido :");
    jLabel2.setToolTipText("");

    jbMembresia.setBackground(new java.awt.Color(249, 133, 42));
    jbMembresia.setForeground(new java.awt.Color(255, 255, 255));
    jbMembresia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
    jbMembresia.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
        jbMembresiaItemStateChanged(evt);
      }
    });

    jButton1.setBackground(new java.awt.Color(249, 133, 42));
    jButton1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton1.setForeground(new java.awt.Color(255, 255, 255));
    jButton1.setText("Guardar");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    jButton2.setBackground(new java.awt.Color(249, 133, 42));
    jButton2.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton2.setForeground(new java.awt.Color(255, 255, 255));
    jButton2.setText("Cancelar");
    jButton2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton2ActionPerformed(evt);
      }
    });

    jLabelNombre.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabelNombre.setForeground(new java.awt.Color(255, 255, 255));
    jLabelNombre.setText("jLabel3");

    jLabelApellidos.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabelApellidos.setForeground(new java.awt.Color(255, 255, 255));
    jLabelApellidos.setText("jLabel4");

    jPanel5.setBackground(new java.awt.Color(249, 133, 42));
    jPanel5.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 200, Short.MAX_VALUE)
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    jPanel6.setBackground(new java.awt.Color(249, 133, 42));
    jPanel6.setPreferredSize(new java.awt.Dimension(200, 2));

    javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
    jPanel6.setLayout(jPanel6Layout);
    jPanel6Layout.setHorizontalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 200, Short.MAX_VALUE)
    );
    jPanel6Layout.setVerticalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    jPanel7.setBackground(new java.awt.Color(49, 49, 49));
    jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

    jLabel3.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(255, 255, 255));
    jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel3.setText("Fecha de termino");

    jLabelFecha.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabelFecha.setForeground(new java.awt.Color(255, 255, 255));
    jLabelFecha.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

    jLabel5.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel5.setForeground(new java.awt.Color(255, 255, 255));
    jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel5.setText("Precio");

    jLabelPrecio.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabelPrecio.setForeground(new java.awt.Color(255, 255, 255));
    jLabelPrecio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

    cantidad_membresia.addChangeListener(new javax.swing.event.ChangeListener() {
      public void stateChanged(javax.swing.event.ChangeEvent evt) {
        cantidad_membresiaStateChanged(evt);
      }
    });

    jLabel7.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel7.setForeground(new java.awt.Color(255, 255, 255));
    jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    jLabel7.setText("Cantidad de membresia");

    javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
    jPanel7.setLayout(jPanel7Layout);
    jPanel7Layout.setHorizontalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel7Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE)
          .addComponent(jLabelFecha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addGap(18, 18, 18)
        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabelPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(17, 17, 17)
        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(cantidad_membresia)
          .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
        .addContainerGap(23, Short.MAX_VALUE))
    );
    jPanel7Layout.setVerticalGroup(
      jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel7Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel3)
          .addComponent(jLabel5)
          .addComponent(jLabel7))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
          .addGroup(jPanel7Layout.createSequentialGroup()
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(jLabelFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(3, 3, 3))
          .addComponent(jLabelPrecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(cantidad_membresia))
        .addContainerGap())
    );

    jLabel4.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
    jLabel4.setForeground(new java.awt.Color(255, 255, 255));
    jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
              .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(rSLabelHora1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(107, 107, 107)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addComponent(jbMembresia, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jLabel2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jLabelApellidos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE))
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(166, 166, 166))))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(rSLabelHora1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(44, 44, 44)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel1)
          .addComponent(jLabelNombre))
        .addGap(18, 18, 18)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2)
          .addComponent(jLabelApellidos))
        .addGap(18, 18, 18)
        .addComponent(jbMembresia, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(18, 18, 18)
        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButton1)
          .addComponent(jButton2))
        .addGap(74, 74, 74))
    );

    jTabbedPane1.addTab("Formulario registro", jPanel1);

<<<<<<< HEAD
    jPanel3.setBackground(new java.awt.Color(49, 49, 49));

    jTable2.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    jScrollPane2.setViewportView(jTable2);

    jButton3.setBackground(new java.awt.Color(0, 255, 125));
    jButton3.setText("jButton3");

    jTextField1.setText("jTextField1");

    jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

    jPanel4.setBackground(new java.awt.Color(0, 255, 125));
    jPanel4.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
          .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
          .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jButton3)
        .addGap(22, 22, 22))
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
          .addComponent(jButton3)
          .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE))
    );

    jTabbedPane1.addTab("Vista de tabla", jPanel3);

=======
>>>>>>> luis
    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jTabbedPane1)
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(0, 0, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    // TODO add your handling code here:
   
    confirmar();
  }//GEN-LAST:event_jButton1ActionPerformed

  private void jbMembresiaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jbMembresiaItemStateChanged
    // TODO add your handling code here:
    fecha();
    id_membresia(evt);
  }//GEN-LAST:event_jbMembresiaItemStateChanged

  private void cantidad_membresiaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_cantidad_membresiaStateChanged
    // TODO add your handling code here:
    cantidades_precio();
  }//GEN-LAST:event_cantidad_membresiaStateChanged

  private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
    // TODO add your handling code here:
    cerrar();
  }//GEN-LAST:event_jButton2ActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
     * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Metal".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(altaClienteMembresia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(altaClienteMembresia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(altaClienteMembresia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(altaClienteMembresia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
        //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new altaClienteMembresia().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JSpinner cantidad_membresia;
  private javax.swing.JButton jButton1;
  private javax.swing.JButton jButton2;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabelApellidos;
  private javax.swing.JLabel jLabelFecha;
  private javax.swing.JLabel jLabelNombre;
  private javax.swing.JLabel jLabelPrecio;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JPanel jPanel6;
  private javax.swing.JPanel jPanel7;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTabbedPane jTabbedPane1;
  private javax.swing.JTable jTable1;
  private javax.swing.JComboBox jbMembresia;
  private rojeru_san.componentes.RSLabelHora rSLabelHora1;
  // End of variables declaration//GEN-END:variables

 
}
