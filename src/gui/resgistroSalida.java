/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import conexionBaseDatos.consultasSalida;
import conexionBaseDatos.multiplesConsultas;
import java.awt.Font;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utilidades.configuracionXml;
import javax.swing.table.DefaultTableModel;
import utilidades.notificaciones;
import utilidades.TextPrompt;
/**
 *
 * @author L
 */
public class resgistroSalida extends javax.swing.JInternalFrame {

  configuracionXml con = new configuracionXml(); 
  multiplesConsultas conexion = new multiplesConsultas( con.getConexion().getConexion() );
  consultasSalida consultasSalida = new consultasSalida( con.getConexion().getConexion() );
  notificaciones notificacion = new notificaciones();
  
  String nombre_tabla = "salida";
  String campos = "id_entrada, fecha, hora";
  
  public resgistroSalida() {
    initComponents();
    selecionarListaSalidas();
    configuracionFrame();
  }
  
  private void configuracionFrame(){
    
    
    TextPrompt mascara_codigo = new TextPrompt("Codigo", codigo);
    mascara_codigo.changeAlpha(0.75f);
    mascara_codigo.changeStyle(Font.BOLD);
  }
  
  private void limpiar(){
   
    codigo.setText("");
    nombre.setText("");
    hora.setText("");
    idEntrada.setText("0");
  }
  
  private void selecionarListaSalidas(){
    
    try {
      
      DefaultTableModel modelo_tabla_salidas = new DefaultTableModel();
      modelo_tabla_salidas = conexion.selecionarListaMulti(nombre_tabla, "*");
      jTableSalida.setModel(modelo_tabla_salidas);
      
    } catch (SQLException ex) {
      notificacion.mensajeError( "No se lograron recuperar datos" );
      Logger.getLogger(resgistroSalida.class.getName()).log(Level.SEVERE, null, ex);
    }
    
  }
  
  
  private void selecion_datos_cliente(){
    
    String selecion = "id_entrada, hora, nombre";
    String consultar = codigo.getText();
      
    try {

      List <Object> values = new ArrayList<Object>();
      
      values = consultasSalida.datos_salida("entrada", selecion, consultar);
      
      idEntrada.setText(values.get(0).toString());
      hora.setText(values.get(1).toString());
      nombre.setText(values.get(2).toString());
      
    } catch (SQLException ex) {
      notificacion.mensajeError( "No se lograron recuperar datos" );
      Logger.getLogger(resgistroSalida.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
  }
  
  
  
  private void guardar(){
    
    Date date = new Date();
    DateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
    DateFormat formatoFecha = new SimpleDateFormat( "yyyy/MM/dd" );
    
    String id_entrada = idEntrada.getText();
    String fecha = formatoFecha.format(date);
    String hora = formatoHora.format(date);
    
    if( (!id_entrada.equals("0")) && (!fecha.equals("")) && ( !hora.equals("") ) ){
    
      try {
        List<Object> valores = new ArrayList<Object>();
        
        valores.add(id_entrada);
        valores.add(fecha);
        valores.add(hora);
        
        boolean resultado = conexion.ingresar_multi(valores, nombre_tabla, campos);
        
        if(resultado){
          
          notificacion.mensajeCorrecto( "Resgistro exitoso" );
          selecionarListaSalidas();
          limpiar();
          
        }else{
          notificacion.mensajeError( "No se lograron guardar los datos" );
        } 
      } catch (SQLException ex) {
        notificacion.mensajeError( "No se logro la insercion de datos" );
        Logger.getLogger(resgistroSalida.class.getName()).log(Level.SEVERE, null, ex);
      }
    
    }else{
    
      notificacion.mensajeAdvertencia( "Los datos estan imcompletos" );
    }
    
  }
  
  
  
  
  

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    jTableSalida = new javax.swing.JTable();
    codigo = new javax.swing.JTextField();
    jPanel2 = new javax.swing.JPanel();
    jButton1 = new javax.swing.JButton();
    rSLabelHora1 = new rojeru_san.componentes.RSLabelHora();
    rSLabelFecha1 = new rojeru_san.componentes.RSLabelFecha();
    jPanel3 = new javax.swing.JPanel();
    jPanel4 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    idEntrada = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    nombre = new javax.swing.JLabel();
    jButton2 = new javax.swing.JButton();
    jButton3 = new javax.swing.JButton();
    jLabel7 = new javax.swing.JLabel();
    hora = new javax.swing.JLabel();
    jLabel9 = new javax.swing.JLabel();
    jButton4 = new javax.swing.JButton();

    jPanel1.setBackground(new java.awt.Color(30, 30, 30));

    jTableSalida.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    jScrollPane1.setViewportView(jTableSalida);

    codigo.setBackground(new java.awt.Color(30, 30, 30));
    codigo.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    codigo.setForeground(new java.awt.Color(255, 255, 255));
    codigo.setBorder(null);
    codigo.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        codigoKeyReleased(evt);
      }
    });

    jPanel2.setBackground(new java.awt.Color(249, 133, 42));
    jPanel2.setPreferredSize(new java.awt.Dimension(200, 3));

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 200, Short.MAX_VALUE)
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 3, Short.MAX_VALUE)
    );

    jButton1.setBackground(new java.awt.Color(79, 182, 95));
    jButton1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
    jButton1.setForeground(new java.awt.Color(255, 255, 255));
    jButton1.setText("Guardar");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    rSLabelHora1.setForeground(new java.awt.Color(255, 255, 255));

    rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));

    jPanel3.setBackground(new java.awt.Color(249, 133, 42));
    jPanel3.setPreferredSize(new java.awt.Dimension(192, 3));

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 192, Short.MAX_VALUE)
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 3, Short.MAX_VALUE)
    );

    jPanel4.setBackground(new java.awt.Color(249, 133, 42));
    jPanel4.setPreferredSize(new java.awt.Dimension(353, 3));

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 353, Short.MAX_VALUE)
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 3, Short.MAX_VALUE)
    );

    jLabel1.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 24)); // NOI18N
    jLabel1.setForeground(new java.awt.Color(255, 255, 255));
    jLabel1.setText("No entrada :");

    idEntrada.setFont(new java.awt.Font("SansSerif", 0, 24)); // NOI18N
    idEntrada.setForeground(new java.awt.Color(255, 255, 255));
    idEntrada.setText("0");

    jLabel3.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 24)); // NOI18N
    jLabel3.setForeground(new java.awt.Color(255, 255, 255));
    jLabel3.setText("Nombre :");

    nombre.setFont(new java.awt.Font("SansSerif", 0, 24)); // NOI18N
    nombre.setForeground(new java.awt.Color(255, 255, 255));

    jButton2.setBackground(new java.awt.Color(124, 221, 187));
    jButton2.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jButton2.setText(" Mostrar todos los datos");

    jButton3.setBackground(new java.awt.Color(204, 0, 0));
    jButton3.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 18)); // NOI18N
    jButton3.setForeground(new java.awt.Color(255, 255, 255));
    jButton3.setText("Limpiar");
    jButton3.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton3ActionPerformed(evt);
      }
    });

    jLabel7.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 24)); // NOI18N
    jLabel7.setForeground(new java.awt.Color(255, 255, 255));
    jLabel7.setText("Hora :");

    hora.setFont(new java.awt.Font("SansSerif", 0, 24)); // NOI18N
    hora.setForeground(new java.awt.Color(255, 255, 255));

    jLabel9.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jLabel9.setForeground(new java.awt.Color(255, 255, 255));
    jLabel9.setText("Clave");

    jButton4.setText("Buscar Clave");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(rSLabelHora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10))
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
            .addGap(0, 154, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                      .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(idEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                  .addComponent(jLabel3)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)))
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110)
                .addComponent(hora, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(71, 71, 71))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(34, 34, 34)))
        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 588, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(rSLabelHora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(54, 54, 54)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(idEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel1))
        .addGap(44, 44, 44)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel7)
          .addComponent(hora))
        .addGap(30, 30, 30)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel3)
          .addComponent(nombre))
        .addGap(62, 62, 62)
        .addComponent(jLabel9)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(11, 11, 11)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButton2)
          .addComponent(jButton4))
        .addGap(70, 70, 70))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    // TODO add your handling code here:
   guardar();
  }//GEN-LAST:event_jButton1ActionPerformed

  private void codigoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codigoKeyReleased
    // TODO add your handling code here:
    selecion_datos_cliente();
  }//GEN-LAST:event_codigoKeyReleased

  private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    // TODO add your handling code here:
    limpiar();
  }//GEN-LAST:event_jButton3ActionPerformed


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JTextField codigo;
  private javax.swing.JLabel hora;
  private javax.swing.JLabel idEntrada;
  private javax.swing.JButton jButton1;
  private javax.swing.JButton jButton2;
  private javax.swing.JButton jButton3;
  private javax.swing.JButton jButton4;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel4;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTableSalida;
  private javax.swing.JLabel nombre;
  private rojeru_san.componentes.RSLabelFecha rSLabelFecha1;
  private rojeru_san.componentes.RSLabelHora rSLabelHora1;
  // End of variables declaration//GEN-END:variables
}
