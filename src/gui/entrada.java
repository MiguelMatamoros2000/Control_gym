/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import utilidades.notificaciones;
import conexionBaseDatos.multiplesConsultas;
import conexionBaseDatos.consultaEntrada;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utilidades.configuracionXml;
import modelo.modelo_membresias;
import utilidades.TextPrompt;

public class entrada extends javax.swing.JInternalFrame {
  
  configuracionXml con = new configuracionXml();
  multiplesConsultas conexion = new multiplesConsultas(con.getConexion().getConexion());
  consultaEntrada _conex = new consultaEntrada(con.getConexion().getConexion());
  notificaciones notificaciones = new notificaciones();
  
  String tabla = "entrada";
  String campos = "nombre,hora,fecha,id_membresia,clave";
  String id_pk = "id_entrada";
  String seleccion = "entrada.*, membresia_datos.nombre, membresia_datos.precio";
  String complemento = "";
  int index_combo = 0;


  
  public entrada() {
    initComponents();
    llenar_combo_membresia();
    configurarJframe();
    mostrar_tabla_lista_entrada();
    llenar_combo_nusqueda();
  }
  
  private void configurarJframe(){
    
    TextPrompt placeholder_nombre = new TextPrompt("Nombre", nombre);
      placeholder_nombre.changeAlpha(0.75f);
      placeholder_nombre.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_clave = new TextPrompt("Clave", clave);
      placeholder_clave.changeAlpha(0.75f);
      placeholder_clave.changeStyle(Font.BOLD);
      
    TextPrompt placeholder_busqueda = new TextPrompt("Buscar", busca);
      placeholder_busqueda.changeAlpha(0.75f);
      placeholder_busqueda.changeStyle(Font.BOLD);
  }
  
  private void limpiar(){
    
    nombre.setText("");
    clave.setText("");
    llenar_combo_membresia();
     btnGuardar.setText("Guardar");
    
  }
  
  private void mostrar_tabla_lista_entrada(){
    
    String _tabla = "entrada INNER JOIN membresia_datos ON (entrada.id_membresia = membresia_datos.id_menbresia_datos)";
    
    try {
      DefaultTableModel modelo_entrada = new DefaultTableModel();
      modelo_entrada = conexion.selecionarListaMulti(_tabla, seleccion);
      tablaEntrada.setModel(modelo_entrada);
      
    } catch (SQLException ex) {
      notificaciones.mensajeError( "No se lograron recuperar los datos" );
      Logger.getLogger(entrada.class.getName()).log(Level.SEVERE, null, ex);
    }
    
  }
  
  private void llenar_combo_membresia(){
    
    _seleccion.removeAllItems();
    modelo_membresias _modelo_membresia = new modelo_membresias(con.getConexion().getConexion());
    DefaultComboBoxModel modelo = new DefaultComboBoxModel( _modelo_membresia.mostrarmodelo() );
    _seleccion.setModel(modelo);
    
  }

  private void seleccionar_membresia_combo(java.awt.event.ItemEvent evt){
    
    if( evt.getStateChange() == ItemEvent.SELECTED ){
      
      modelo_membresias _modelo_membresia =  (modelo_membresias) _seleccion.getSelectedItem();
      index_combo = _modelo_membresia.getId();
      
    }
    
  }
  
  private void llenar_combo_nusqueda(){
    
    campo_busca.addItem("Seleccionar campo");
    campo_busca.addItem("membresia_datos.nombre");
    campo_busca.addItem("membresia_datos.precio");
    campo_busca.addItem("entrada.id_entrada");
    campo_busca.addItem("entrada.nombre");
    campo_busca.addItem("entrada.hora");
    campo_busca.addItem("entrada.fecha");
    campo_busca.addItem("entrada.clave");
    campo_busca.addItem("entrada.id_membresia");
    
  }
  
  private void confirmar_registro(){
    
    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int respuesta = JOptionPane.showConfirmDialog(null, " ¿ Estas seguro de esta accion ?", " Registrar ", dialogo);
    
    if(respuesta == 0){
      guardar();
    }
    
    
  }
  
  private void guardar(){
    
    tabla = "entrada";
    Date date = new Date();
    DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    String hora = hourFormat.format(date);
    String fecha = dateFormat.format(date);
    String _nombre = nombre.getText();
    String _clave = clave.getText();
    
    if( (!hora.equals("")) && (!fecha.equals("")) && (!_nombre.equals("")) && (!_clave.equals("")) ){
      
      
      try {
        
        int comprovacion_resultado = _conex.comprovar_clave_cliente(_clave);
        int comprovar_clave_entrada = _conex.comprovar_clave_entrada(_clave);
        
        if( (comprovacion_resultado == 0) && ( comprovar_clave_entrada == 0 ) ){
          
        List <Object> values = new ArrayList<Object>();
        values.add(_nombre);
        values.add(hora);
        values.add(fecha);
        values.add(index_combo);
        values.add(_clave);
        
        
        boolean resultado = conexion.ingresar_multi(values, tabla, campos);
        
        if(resultado){
          
          notificaciones.mensajeCorrecto( "Entrada Registrada" );
          limpiar();
          mostrar_tabla_lista_entrada();
          
        }else{
          notificaciones.mensajeError("Sus datos no son correctos");
        }
        
        }else{
          
          
          notificaciones.mensajeAdvertencia( "La calve ya existe" );
          
        }
        
      } catch (SQLException ex) {
        
        notificaciones.mensajeError( "No se logro realizar la accion" );
        Logger.getLogger(entrada.class.getName()).log(Level.SEVERE, null, ex);
      }
      
    }else{
      
      notificaciones.mensajeAdvertencia("Comprueve que sus datos esten completos");
      
    }
    
  }
  
  
  private void confirmar_eliminacion(){
    
    
    int dialogo = JOptionPane.YES_NO_OPTION;
    int respuesta = JOptionPane.showConfirmDialog(null, " ¿ Estas seguro de esta accion ?", " Eliminar ", dialogo);
    
    if(respuesta == 0){
      eliminar();
    }
    
    
  }
  
  private void eliminar() { 
    tabla = "entrada";
    try {
    
      int fila = tablaEntrada.getSelectedRow();
      if( fila >= 0 ){
        
        DefaultTableModel modeloTablaCliente = (DefaultTableModel) tablaEntrada.getModel();
        int id_fila = Integer.parseInt( modeloTablaCliente.getValueAt(fila, 0).toString() );
        boolean resultado = conexion.Elimnar(id_fila, tabla, id_pk);
        
        if( resultado )
        {
          
          mostrar_tabla_lista_entrada();
          limpiar();
          notificaciones.mensajeCorrecto("La entrada fue eliminada");
          
        }else{
          
          notificaciones.mensajeError("No se logro eliminar la entrada");
          
        }

      }
            
    } catch (Exception e) {
      
      notificaciones.mensajeError("No se lograron recuperar los datos");
      
    }
    
  }
  
  
  private void selecionar_entrada(){
    
    int fila = tablaEntrada.getSelectedRow();
    if ( fila >= 0 ) 
    {
      try 
      {
        DefaultTableModel modelo_tabla_entrada = (DefaultTableModel)tablaEntrada.getModel();
        int id_fila = Integer.parseInt( modelo_tabla_entrada.getValueAt( fila, 0 ).toString() );
        java.util.List<Object> entrada = conexion.SeleccionObjeto(id_fila, tabla, id_pk,  campos);
        
        if( entrada.size() > 0 )
        {
          Map<String, Object> map = ( ( Map<String, Object> ) ( entrada.get( 0 ) ) );
          String _nombre = map.get( "nombre").toString();
          String _clave = map.get( "clave" ).toString();
                  
          nombre.setText(_nombre);
          clave.setText(_clave);
          btnGuardar.setText("Guardar Cambios");
        }
      } 
      catch (SQLException ex) 
      {
        notificaciones.mensajeError("No se lograron recuperar los datos");
        Logger.getLogger(cliente.class.getName()).log(Level.SEVERE, null, ex);
      }
      
   }
    
  }
  
  private void compara_boton(){
    
    String texto_btn = btnGuardar.getText();
    
    if(  texto_btn.equals("Guardar")){
      
      confirmar_registro();
      
    }else{
      
      editar();
      
    }
    
    
  }
  
  
  private void editar(){
    
    String _campos = "nombre = ?,hora = ?,fecha = ?,id_membresia = ?,clave = ?";
    String texto_btn = btnGuardar.getText();
            
    if(  texto_btn.equals( "Guardar Cambios" ) ){
      
      Date date = new Date();
      DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
      String hora = hourFormat.format(date);
      String fecha = dateFormat.format(date);
      String _nombre = nombre.getText();
      String _clave = clave.getText();
      
      if ( (!hora.equals("")) && (!fecha.equals("")) && (!_nombre.equals("")) && (!_clave.equals("")) ){
        
        
        
        
      int fila = tablaEntrada.getSelectedRow();
      
      try {
        
        if( fila >= 0 ){
          
          DefaultTableModel modelo_entrada = (DefaultTableModel) tablaEntrada.getModel();
          int id_fila = Integer.parseInt( modelo_entrada.getValueAt(fila, 0).toString() );
          
          List<Object> values = new ArrayList<>();
          values.add(_nombre);
          values.add(hora);
          values.add(fecha);
          values.add(index_combo);
          values.add(_clave);
          values.add(id_fila);
          
          boolean resultado = conexion.actualizar_datos(values, tabla, id_pk, _campos);
          
          if(resultado){
            
            btnGuardar.setText("Guardar");
            limpiar();
            notificaciones.mensajeCorrecto( "La Actualizacion fue correcta" );
            mostrar_tabla_lista_entrada();
            
          }else{
            
            notificaciones.mensajeError( "No se logro actualizar" );
            
          }
          
        }
        
      } catch (Exception e) {
        
        notificaciones.mensajeError( "No se recuperaron los datos" );
        
      }
    }
    }
  }
  
  
  private void buscar(){
    
    String _busqueda = busca.getText();
    String campo = campo_busca.getSelectedItem().toString();
    try {
    if( !_busqueda.equals("") || !campo.equals("Seleccionar campo") ){
      
      
      String _tabla = "entrada INNER JOIN membresia_datos ON (entrada.id_membresia = membresia_datos.id_menbresia_datos)";
    
     
        DefaultTableModel modelo_entrada = new DefaultTableModel();
        modelo_entrada = conexion.busqueda(_tabla, campo, _busqueda, seleccion, complemento);
        tablaEntrada.setModel(modelo_entrada);
      
      
  
    }else{
      notificaciones.mensajeAdvertencia( "Seleccione un campo de busqueda o coloque su busqueda" );
    }
    } catch (SQLException ex) {
        notificaciones.mensajeError( "No se lograron recuperar los datos" );
        Logger.getLogger(entrada.class.getName()).log(Level.SEVERE, null, ex);
      }

  }
  
  
  private void memebresia(){
    
    String _clave_1 = JOptionPane.showInputDialog("Ingrese su clave"); 
    System.out.println(_clave_1);
    
    if( !_clave_1.equals("") ){
      
      try {
        String estatus = _conex.cosnultar_estatus(_clave_1);
        
        if(estatus.equals("Activa")){
          try {
            
            
            
            List <Object> values = new ArrayList<Object>();
            
            values = _conex.Seleccion_membresia(_clave_1);
            
            boolean resultado = conexion.ingresar_multi(values, tabla, campos);
            
            if(resultado){
              
              notificaciones.mensajeCorrecto( "Entrada Registrada" );
              limpiar();
              mostrar_tabla_lista_entrada();
              
            }else{
              notificaciones.mensajeError("Sus datos no son correctos");
            }
          } catch (SQLException ex) {
            
            notificaciones.mensajeError( "No se logro realizar la accion" );
            Logger.getLogger(entrada.class.getName()).log(Level.SEVERE, null, ex);
          }

        }else{
          
          notificaciones.mensajeAdvertencia("La clave es incorrecta o la membresia no se encuentra activa");
          
        }
      } catch (SQLException ex) {
        notificaciones.mensajeError( "No se encontraron los datos" );
        Logger.getLogger(entrada.class.getName()).log(Level.SEVERE, null, ex);
      }
    }else{
      notificaciones.mensajeAdvertencia("Deve colocar la clave para poder continuar" );
    }
  }
  
  
  
  
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    menuOpcciones = new javax.swing.JPopupMenu();
    jMenuItemEditar = new javax.swing.JMenuItem();
    jMenuItemEliminar = new javax.swing.JMenuItem();
    jPanel1 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tablaEntrada = new javax.swing.JTable();
    campo_busca = new javax.swing.JComboBox();
    busca = new javax.swing.JTextField();
    jPanel2 = new javax.swing.JPanel();
    btnGuardar = new javax.swing.JButton();
    btnLimpiar = new javax.swing.JButton();
    jButton4 = new javax.swing.JButton();
    nombre = new javax.swing.JTextField();
    jPanel3 = new javax.swing.JPanel();
    _seleccion = new javax.swing.JComboBox();
    clave = new javax.swing.JTextField();
    jPanel4 = new javax.swing.JPanel();
    rSLabelHora1 = new rojeru_san.componentes.RSLabelHora();
    rSLabelFecha1 = new rojeru_san.componentes.RSLabelFecha();
    jPanel5 = new javax.swing.JPanel();
    jPanel6 = new javax.swing.JPanel();

    jMenuItemEditar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEditar.setText("Editar");
    jMenuItemEditar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEditarActionPerformed(evt);
      }
    });
    menuOpcciones.add(jMenuItemEditar);

    jMenuItemEliminar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    jMenuItemEliminar.setText("Eliminar");
    jMenuItemEliminar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jMenuItemEliminarActionPerformed(evt);
      }
    });
    menuOpcciones.add(jMenuItemEliminar);

    jPanel1.setBackground(new java.awt.Color(30, 30, 30));
    jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

    jScrollPane1.setBackground(new java.awt.Color(30, 30, 30));
    jScrollPane1.setForeground(new java.awt.Color(255, 255, 255));

    tablaEntrada.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null},
        {null, null, null, null}
      },
      new String [] {
        "Title 1", "Title 2", "Title 3", "Title 4"
      }
    ));
    tablaEntrada.setComponentPopupMenu(menuOpcciones);
    jScrollPane1.setViewportView(tablaEntrada);

    campo_busca.setBackground(new java.awt.Color(249, 133, 42));
    campo_busca.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
    campo_busca.setForeground(new java.awt.Color(255, 255, 255));

    busca.setBackground(new java.awt.Color(30, 30, 30));
    busca.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    busca.setForeground(new java.awt.Color(255, 255, 255));
    busca.setBorder(null);
    busca.setCaretColor(new java.awt.Color(255, 255, 255));
    busca.addKeyListener(new java.awt.event.KeyAdapter() {
      public void keyReleased(java.awt.event.KeyEvent evt) {
        buscaKeyReleased(evt);
      }
    });

    jPanel2.setBackground(new java.awt.Color(249, 133, 42));
    jPanel2.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    btnGuardar.setBackground(new java.awt.Color(249, 133, 42));
    btnGuardar.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 14)); // NOI18N
    btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
    btnGuardar.setText("Guardar");
    btnGuardar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnGuardarActionPerformed(evt);
      }
    });

    btnLimpiar.setBackground(new java.awt.Color(249, 133, 42));
    btnLimpiar.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
    btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
    btnLimpiar.setText("Limpiar");
    btnLimpiar.setToolTipText("");
    btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnLimpiarActionPerformed(evt);
      }
    });

    jButton4.setBackground(new java.awt.Color(249, 133, 42));
    jButton4.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
    jButton4.setForeground(new java.awt.Color(255, 255, 255));
    jButton4.setText("Cuenta con membresia");
    jButton4.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton4ActionPerformed(evt);
      }
    });

    nombre.setBackground(new java.awt.Color(30, 30, 30));
    nombre.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    nombre.setForeground(new java.awt.Color(255, 255, 255));
    nombre.setBorder(null);
    nombre.setCaretColor(new java.awt.Color(255, 255, 255));
    nombre.setPreferredSize(new java.awt.Dimension(59, 33));

    jPanel3.setBackground(new java.awt.Color(249, 133, 42));
    jPanel3.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    _seleccion.setBackground(new java.awt.Color(249, 133, 42));
    _seleccion.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
    _seleccion.setForeground(new java.awt.Color(255, 255, 255));
    _seleccion.setMinimumSize(new java.awt.Dimension(56, 33));
    _seleccion.setPreferredSize(new java.awt.Dimension(56, 33));
    _seleccion.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
        _seleccionItemStateChanged(evt);
      }
    });

    clave.setBackground(new java.awt.Color(30, 30, 30));
    clave.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
    clave.setForeground(new java.awt.Color(255, 255, 255));
    clave.setBorder(null);
    clave.setCaretColor(new java.awt.Color(255, 255, 255));
    clave.setPreferredSize(new java.awt.Dimension(59, 33));

    jPanel4.setBackground(new java.awt.Color(249, 133, 42));
    jPanel4.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    rSLabelHora1.setForeground(new java.awt.Color(255, 255, 255));

    rSLabelFecha1.setForeground(new java.awt.Color(255, 255, 255));

    jPanel5.setBackground(new java.awt.Color(249, 133, 42));
    jPanel5.setPreferredSize(new java.awt.Dimension(0, 3));

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 0, Short.MAX_VALUE)
    );
    jPanel5Layout.setVerticalGroup(
      jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    jPanel6.setBackground(new java.awt.Color(249, 133, 42));
    jPanel6.setPreferredSize(new java.awt.Dimension(0, 2));

    javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
    jPanel6.setLayout(jPanel6Layout);
    jPanel6Layout.setHorizontalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 200, Short.MAX_VALUE)
    );
    jPanel6Layout.setVerticalGroup(
      jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 2, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE))
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
            .addGap(164, 164, 164)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
              .addComponent(clave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(_seleccion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
              .addComponent(nombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(87, 87, 87))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                  .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                  .addComponent(rSLabelHora1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)))
              .addComponent(campo_busca, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE)
              .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addComponent(rSLabelFecha1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
              .addComponent(busca, javax.swing.GroupLayout.Alignment.TRAILING))))
        .addGap(18, 18, 18)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 579, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jScrollPane1)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(campo_busca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(busca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(28, 28, 28)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(rSLabelHora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(rSLabelFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(33, 33, 33)
        .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(52, 52, 52)
        .addComponent(_seleccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(52, 52, 52)
        .addComponent(clave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(36, 36, 36)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(btnGuardar)
          .addComponent(btnLimpiar))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 13, Short.MAX_VALUE)
        .addComponent(jButton4)
        .addGap(23, 23, 23))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
    // TODO add your handling code here:
    
    compara_boton();
   
  }//GEN-LAST:event_btnGuardarActionPerformed

  private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    // TODO add your handling code here:
    limpiar();
  }//GEN-LAST:event_btnLimpiarActionPerformed

  private void _seleccionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event__seleccionItemStateChanged
    // TODO add your handling code here:
    seleccionar_membresia_combo(evt);
  }//GEN-LAST:event__seleccionItemStateChanged

  private void jMenuItemEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarActionPerformed
    // TODO add your handling code here:
    confirmar_eliminacion();
  }//GEN-LAST:event_jMenuItemEliminarActionPerformed

  private void jMenuItemEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEditarActionPerformed
    // TODO add your handling code here:
    selecionar_entrada();
  }//GEN-LAST:event_jMenuItemEditarActionPerformed

  private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
    // TODO add your handling code here:
    memebresia();
  }//GEN-LAST:event_jButton4ActionPerformed

  private void buscaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buscaKeyReleased
    // TODO add your handling code here:
    buscar();
  }//GEN-LAST:event_buscaKeyReleased


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JComboBox _seleccion;
  private javax.swing.JButton btnGuardar;
  private javax.swing.JButton btnLimpiar;
  private javax.swing.JTextField busca;
  private javax.swing.JComboBox campo_busca;
  private javax.swing.JTextField clave;
  private javax.swing.JButton jButton4;
  private javax.swing.JMenuItem jMenuItemEditar;
  private javax.swing.JMenuItem jMenuItemEliminar;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel4;
  private javax.swing.JPanel jPanel5;
  private javax.swing.JPanel jPanel6;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JPopupMenu menuOpcciones;
  private javax.swing.JTextField nombre;
  private rojeru_san.componentes.RSLabelFecha rSLabelFecha1;
  private rojeru_san.componentes.RSLabelHora rSLabelHora1;
  private javax.swing.JTable tablaEntrada;
  // End of variables declaration//GEN-END:variables
}
