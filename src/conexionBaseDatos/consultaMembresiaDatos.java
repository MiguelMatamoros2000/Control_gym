/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionBaseDatos;

import java.sql.*;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

public class consultaMembresiaDatos extends conexionBaseDatos {
  
  public consultaMembresiaDatos (Connection _conexion){
    
    conexion = _conexion;
    
  }

  @Override
  public boolean ingresar(Map<String, Object> values) throws SQLException {
    
    String nombre = "membresia_datos";
    String columnas = "descripcion, precio, nombre, dias";
    String valores = "?,?,?,?";
    
    String sql = "INSERT INTO " + nombre + "("+ columnas +")"+ " VALUES (" + valores + ")";
    
    PreparedStatement declaracion = conexion.prepareStatement(sql);
      declaracion.setObject(1, values.get("nombre"));
      declaracion.setObject(2, values.get("precio"));
      declaracion.setObject(3, values.get("dias"));
      declaracion.setObject(4, values.get("decripcion"));
      
    int filas_agregadas = declaracion.executeUpdate();
    declaracion.close();
    return filas_agregadas > 0;
  }

  @Override
  public boolean borrar(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public boolean actualizar(int id_fila, Map<String, Object> values) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public DefaultTableModel selecionarLista() throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public List<Object> Seleccion(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
  
  
}
