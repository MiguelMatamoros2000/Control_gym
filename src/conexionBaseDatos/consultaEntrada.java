/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionBaseDatos;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author L
 */
public class consultaEntrada extends conexionBaseDatos {
  
  public consultaEntrada(Connection _conexion){
    conexion = _conexion;
  }

  @Override
  public boolean ingresar(Map<String, Object> values) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public boolean borrar(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public boolean actualizar(int id_fila, Map<String, Object> values) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public DefaultTableModel selecionarLista() throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public List<Object> Seleccion(int id_fila) throws SQLException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
  
  public String cosnultar_estatus(String clave) throws SQLException{
    
    String estatus = "";
    
    
    String seleccionar = " membresia.estatus ";
    String tabla = "cliente INNER JOIN membresia ON ( membresia.id_cliente = cliente.id_cliente )";
    
    String sql_query = (
      "SELECT " + 
      seleccionar + " " +
      "FROM " + tabla + " WHERE cliente.clave_llave_secundaria like '"+ clave + "'"
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    if(resultado.next()){
      estatus = resultado.getString( "membresia.estatus" ) ;
    }
    
    return estatus;
  }
  
  public List<Object> Seleccion_membresia( String clave ) throws SQLException {
    
    List<Object> lista_cliente = new ArrayList<>();
    
    java.util.Date date = new java.util.Date();
    DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    String hora = hourFormat.format(date);
    String fecha = dateFormat.format(date);
    
    String seleccionar = "cliente.nombre, cliente.apellido, cliente.clave_llave_secundaria"
                     + ", membresia_datos.id_menbresia_datos";
    String tabla = "cliente INNER JOIN membresia on (cliente.id_cliente = membresia.id_cliente) "
                 + "INNER JOIN membresia_datos ON (membresia.id_membresia_datos = membresia_datos.id_menbresia_datos)";
    
    String sql_query = (
      "SELECT " + 
      seleccionar + " " +
      "FROM " + tabla + " WHERE cliente.clave_llave_secundaria like '"+ clave + "'"
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    if( resultado.next() ){
      
      lista_cliente.add(resultado.getString("cliente.nombre") + " " + resultado.getString("cliente.apellido"));
      lista_cliente.add(hora);
      lista_cliente.add(fecha);
      lista_cliente.add(resultado.getInt("membresia_datos.id_menbresia_datos"));
      lista_cliente.add(clave);
      
      
    }
    
    return lista_cliente; 
  }
  
  public int comprovar_clave_cliente(String clave)throws SQLException{
    
    int resultadoEnvio = 0;
    
    String seleccionar = "COUNT(cliente.clave_llave_secundaria)";
    String tabla = "cliente";
    
    String sql_query = (
      "SELECT " + 
      seleccionar + " " +
      "FROM " + tabla + " WHERE cliente.clave_llave_secundaria like '"+ clave + "'"
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    if(resultado.next()){
      
      resultadoEnvio = resultado.getInt("COUNT(cliente.clave_llave_secundaria)");
      
    }else{
      resultadoEnvio = 0;
    }
    
    
    return resultadoEnvio;
    
  } 
  
  public int comprovar_clave_entrada(String clave)throws SQLException{
    
    int resultadoEnvio = 0;
    
    String seleccionar = "COUNT(entrada.clave)";
    String tabla = "entrada";
    
    String sql_query = (
      "SELECT " + 
      seleccionar + " " +
      "FROM " + tabla + " WHERE entrada.clave like '"+ clave + "'"
    );
    
    PreparedStatement declaracion = conexion.prepareStatement(sql_query);
    ResultSet resultado = declaracion.executeQuery();
    
    if(resultado.next()){
      
      resultadoEnvio = resultado.getInt("COUNT(entrada.clave)");
      
    }else{
      resultadoEnvio = 0;
    }
    
    
    return resultadoEnvio;
    
  } 
  
}
