/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/
package conexionBaseDatos;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;


public class consultasTablaClientes extends conexionBaseDatos {
  
  public consultasTablaClientes( Connection _conexion ){
    conexion = _conexion;
  }
  
  SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

  @Override
  public boolean ingresar(Map<String, Object> values) throws SQLException {
   
    String nombre_tabla = "cliente";
    String columnas = "nombre,apellido,foto,correo,telefono,"
                    + "telefono_emergencia,clave_llave_secundaria,red_social";
    String valores = "?, ?, ?, ?, ?, ?, ?, ?";

    String sql_query = ("INSERT INTO " + nombre_tabla + " (" + columnas + ") VALUES " + "(" + valores + ")"
    );

    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    declaracion.setObject(1, values.get( "nombre" ).toString());
    declaracion.setObject(2, values.get( "apellido" ).toString());
    declaracion.setObject(3, values.get( "imagen" )); 
    declaracion.setObject(4, values.get( "correo" ).toString());
    declaracion.setObject(5, values.get( "telefono" ).toString());
    declaracion.setObject(6, values.get( "telefono_emergencia" ).toString());      
    declaracion.setObject(7, values.get( "clave_llave_secundaria" ).toString());
    declaracion.setObject(8, values.get( "red_social" ).toString()); 
    

    int filas_agregadas = declaracion.executeUpdate();
    declaracion.close();
    return filas_agregadas > 0;
  }
  

  @Override
  public boolean borrar(int id_fila) throws SQLException {
    
    String nombreTabla = "cliente";
    String tabla_pk = "id_cliente";
    
    String sql_query = ( "DELETE " + "FROM " + nombreTabla + " WHERE " + tabla_pk + " = ?" );
    
    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    declaracion.setInt(1, id_fila);
    
    int fila_eliminar = declaracion.executeUpdate();
    declaracion.close();
    
    return fila_eliminar > 0;
    
  }

  @Override
  public boolean actualizar(int id_fila, Map<String, Object> values) throws SQLException {
    
    String nombre_tabla = "cliente";
    String tabla_pk = "id_cliente";
    String columnas_valores = "nombre = ?,apellido = ?,foto = ? ,correo = ?,telefono = ?,"
                            + "telefono_emergencia = ?,clave_llave_secundaria = ?,red_social = ?";

    String sql_query = (
      "UPDATE " + 
      nombre_tabla + " " +
      "SET " + columnas_valores + " " +
      "WHERE " + tabla_pk + " = ?" 
    );
    
    PreparedStatement declaracion = conexion.prepareStatement( sql_query );

    declaracion.setObject(1, values.get( "nombre" ).toString());
    declaracion.setObject(2, values.get( "apellido" ).toString());
    declaracion.setObject(3, values.get( "imagen" )); 
    declaracion.setObject(4, values.get( "correo" ).toString());
    declaracion.setObject(5, values.get( "telefono" ).toString());
    declaracion.setObject(6, values.get( "telefono_emergencia" ).toString());      
    declaracion.setObject(7, values.get( "clave_llave_secundaria" ).toString());
    declaracion.setObject(8, values.get( "red_social" ).toString()); 
    declaracion.setInt( 9, id_fila );

    int filas_actualizadas = declaracion.executeUpdate();
    declaracion.close();
    return filas_actualizadas > 0;
  }

  @Override
  public DefaultTableModel selecionarLista() throws SQLException {
    
    String  string_select = "  membresia.id_membresia,cliente.id_cliente,cliente.nombre,cliente.apellido"
                        + ",cliente.clave_llave_secundaria,membresia.fecha,"
                        + "membresia.fecha_fin,membresia.estatus,membresia_datos.nombre ";
    String nombre_tabla = " cliente INNER JOIN membresia ON (cliente.id_cliente = membresia.id_cliente) "
                         + "INNER JOIN membresia_datos ON (membresia.id_membresia_datos = membresia_datos.id_menbresia_datos)";

    String sql_query = (
      "SELECT " + 
      string_select + " " +
      "FROM " + nombre_tabla +" GROUP BY membresia.id_membresia"
    );
      
      

    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    ResultSet resultado = declaracion.executeQuery();

    //Pasar de ResultSet a DefaultTableModel
    DefaultTableModel modelo = new DefaultTableModel()
    {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    utilidades.ConversorResultSetADefaultTableModel.rellena(resultado, modelo );
    //
    declaracion.close();
    resultado.close();
    return modelo;
    
  }
  
  
  public DefaultTableModel selecionarListaClientes() throws SQLException {
    
    String  string_select = " cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.correo,cliente.telefono, "
                          + "cliente.telefono_emergencia,cliente.clave_llave_secundaria,"
                          + "cliente.red_social";
    String nombre_tabla = " cliente ";

    String sql_query = (
      "SELECT " + 
      string_select + " " +
      "FROM " + nombre_tabla
    );
      
      System.out.println(sql_query);

    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    ResultSet resultado = declaracion.executeQuery();

    //Pasar de ResultSet a DefaultTableModel
    DefaultTableModel modelo = new DefaultTableModel()
    {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    utilidades.ConversorResultSetADefaultTableModel.rellena(resultado, modelo );
    //
    declaracion.close();
    resultado.close();
    return modelo;
    
  }

  @Override
  public List<Object> Seleccion(int id_fila) throws SQLException {
    
    String nombre_tabla = "cliente";
    String tabla_pk = "id_cliente";
    String string_select = "*";

    String sql_query =(
      "SELECT " + 
      string_select + " " +
      "FROM " + nombre_tabla + " "+
      "WHERE " + tabla_pk + " = ?" 
    );
    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    declaracion.setInt( 1, id_fila );

    ResultSet resultado = declaracion.executeQuery();
    ResultSetMetaData md = resultado.getMetaData();
    int columnas = md.getColumnCount();

    List<Object> fila_lista = new ArrayList<Object>();
    while ( resultado.next() ) 
    {
      Map<String, Object> fila = new HashMap<String, Object>();
      for ( int i = 1 ; i <= columnas ; i++ ) 
      {
        fila.put( md.getColumnLabel(i).toLowerCase(), resultado.getObject(i) );
      }
      fila_lista.add( fila );
    }

    declaracion.close();
    resultado.close();
    return fila_lista;
    
  }
  
public String seleccionar_id(Map<String,Object> values){
    
    
    String id_cliente = "";
    try {
      String nombre_tabla = "cliente";
      String seleccionar = "id_cliente";
      
      
      
      String sql_query = ("Select " + seleccionar + " From " + nombre_tabla +
              " where clave_llave_secundaria = '" + values.get("clave_llave_secundaria").toString() + "';");
      
      System.out.println(sql_query);
      
      PreparedStatement declaracion = conexion.prepareStatement( sql_query );
      ResultSet resultado = declaracion.executeQuery();
      
      while(resultado.next()){
        
        id_cliente = resultado.getString("id_cliente");
        
      }
      System.out.println(id_cliente);
      declaracion.close();
      resultado.close();
      
    } catch (SQLException ex) {
      id_cliente = "0";
      Logger.getLogger(consultasTablaClientes.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return id_cliente;
}


public void actualizar_estatus(){
  
  int dia = 0;
  
  Calendar _fecha = Calendar.getInstance();
  _fecha.add(Calendar.DATE,dia);
  
  String fecha = String.format("%1$tY/%1$tm/%1$td", _fecha.getTime());
  
  Date fecha_recuperar = _fecha.getTime();
  long fecha_reconvercion = fecha_recuperar.getTime();
  java.sql.Date fecha_sql = new java.sql.Date(fecha_reconvercion);
  String fecha_consulta = fecha_sql.toString();
  System.out.println(fecha_consulta);
  
  String nombre_tabla = "membresia";
  String campos_actualizar = "estatus";
  String campos = "fecha_fin";
  
  String sql_query = "UPDATE " + nombre_tabla + " SET " + campos_actualizar + " = 'Desactivo' WHERE " 
         + campos + " like " + "'" +fecha_consulta +"'" ;
  
  
  System.out.println(sql_query);
  
    try {
      
      PreparedStatement declaracion = conexion.prepareStatement(sql_query);
      
      
    int filas_actualizadas = declaracion.executeUpdate();
    declaracion.close();
      
    } catch (SQLException ex) {
      Logger.getLogger(consultasTablaClientes.class.getName()).log(Level.SEVERE, null, ex);
    }
  
  
  
}

public boolean borrar_membresia(int id_fila) throws SQLException {
    String nombreTabla = "membresia";
    String tabla_pk = "id_membresia";
    
    String sql_query = ( "DELETE " + "FROM " + nombreTabla + " WHERE " + tabla_pk + " = ?" );
    
    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    declaracion.setInt(1, id_fila);
    
    int fila_eliminar = declaracion.executeUpdate();
    declaracion.close();
    
    return fila_eliminar > 0;
}


public DefaultTableModel busqueda(int opcion, String campo, String busqueda ) throws SQLException {
  
  String  string_select = "";
  String nombre_tabla = "";
  String sql_query = "";
  String complemento = "";
  
  if(opcion == 0){
    
    string_select = " cliente.id_cliente,cliente.nombre,cliente.apellido,cliente.correo,cliente.telefono, "
                          + "cliente.telefono_emergencia,cliente.clave_llave_secundaria,"
                          + "cliente.red_social";
    nombre_tabla = " cliente ";
    complemento = "";
    
  }else{
    
    string_select = "  membresia.id_membresia,cliente.id_cliente,cliente.nombre,cliente.apellido"
                        + ",cliente.clave_llave_secundaria,membresia.fecha,"
                        + "membresia.fecha_fin,membresia.estatus,membresia_datos.nombre ";
    nombre_tabla = " cliente INNER JOIN membresia ON (cliente.id_cliente = membresia.id_cliente) "
                         + "INNER JOIN membresia_datos ON (membresia.id_membresia_datos = membresia_datos.id_menbresia_datos)";
    complemento = "GROUP BY membresia.id_membresia";
  }

    sql_query = (
      "SELECT " + 
      string_select + " " +
      "FROM " + nombre_tabla + " WHERE " + campo + " like '"+ busqueda + "%'" + complemento
    );
      

    PreparedStatement declaracion = conexion.prepareStatement( sql_query );
    ResultSet resultado = declaracion.executeQuery();

    //Pasar de ResultSet a DefaultTableModel
    DefaultTableModel modelo = new DefaultTableModel()
    {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    utilidades.ConversorResultSetADefaultTableModel.rellena(resultado, modelo );
    //
    declaracion.close();
    resultado.close();
    return modelo;
  }
  
  
}


